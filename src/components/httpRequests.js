fetch('https:/dashboard.com/endpoint/', {
  method: 'POST',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({firstParam: 'Value', secondParam: 'secondOtherValue'})
})

import axios from 'axios'

axios
  .get('https://api.stephen.africam/users/user/id')
  .then(response => this.setState({username: response.data.name}))

const user = {
  name: this.state.name
};

axios
  .post(`https://api.stephen.africam.com/users`, {user})
  .then(res => {
    console.log(res);
    console.log(res.data);
  })

axios
  .delete(`https://api.stephen.africam/users/${this.state.id}`)
  .then(res => {
    console.log(res);
    console.log(res.data);
  })

// Omitted
import API from '../components/Constants';

API
  .delete(`users/${this.state.id}`)
  .then(res => {
    console.log(res);
    console.log(res.data);
  })

//with async and wait

handleSubmit = async event => {
  event.preventDefault();

  // Promise is resolved and value is inside of the response const.
  const response = await API.delete(`users/${this.state.id}`);

  console.log(response);
  console.log(response.data);
};

//https://alligator.io/react/axios-react/