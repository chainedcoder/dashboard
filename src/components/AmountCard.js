import React from 'react';
import Card from '@material-ui/core/Card';
import STimeAgo from '../components/STimeAgo';
import moment from 'moment';
class AmountCard extends React.Component {
  constructor() {
    super();
    this.state = {
      someKey: 'someValue'
    };
  }

  render() {
    var fDate = moment(this.props.date, "DD/MM/YYYY h:mm:ss").format('lll');
    return (
      <div>
        <Card className={"amount-container "}>
          <div className={"amount-card " + this.props.className}>
            <div className="amount-card-title">{this.props.title}</div>
            <div className="amount-card-amt">  {this.props.amount}
            </div>
            <div className="tooltip">
                    {/* <span className="tooltiptext">{fDate}</span> */}
                      Transactions
                      {/* <STimeAgo date={this.props.date}></STimeAgo> */}
                    </div>

          </div>
        </Card>
      </div>
    );
  }

  componentDidMount() {
    this.setState({someKey: 'otherValue'});
  }
}

export default AmountCard;
