import React, {Component} from 'react'
import Input from './Input'
// import Button from './Button' import FormErrors from './FormErrors'
import {Redirect} from 'react-router-dom'
// import SimpleInput from './SimpleInput';
import ValidationErrors from './ValidationErrors'
import {withRouter} from 'react-router'
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as sessionActions from '../actions/sessionActions';
import API, {ENDPOINTS} from '../Api/Constants'
import moment from 'moment'
import store from '../redux/index'
// import {removeNotificationBar} from '../redux/actions.js'
import Grid from '@material-ui/core/Grid';
import {withStyles} from '@material-ui/core/styles';

const styles = theme => ({
  sectionDesktop: {
    display: 'none',
    [
      theme
        .breakpoints
        .up('md')
    ]: {
      display: 'flex'
    }
  },
  sectionLargeDesktop: {
    display: 'none',
    [
      theme
        .breakpoints
        .up('lg')
    ]: {
      display: 'flex'
    }
  },
  sectionMobile: {
    display: 'flex',
    [
      theme
        .breakpoints
        .up('md')
    ]: {
      display: 'none'
    }
  }
})

class LogIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formValid: false,
      animate: false,
      waitForResponse: false,
      emailError: '',
      passError: '',
      passwordValue: '',
      passwordValid: true,
      signupRedirect: false,
      showNotificationBar: true
    };

    this.handleBtnAnimate = this
      .handleBtnAnimate
      .bind(this);

    this.onInputPass = this
      .onInputPass
      .bind(this)

    this.onInputEmail = this
      .onInputEmail
      .bind(this)

    this.setSignupRedirect = this
      .setSignupRedirect
      .bind(this)
  }

  setSignupRedirect = () => {
    this.setState({signupRedirect: true})
  }

  goToSignup = () => {
    if (this.state.signupRedirect) {
      return <Redirect to='/sign-up'/>
    }
  }

  login = (user, history) => {
    API
      .post(ENDPOINTS.login, JSON.stringify(user))
      .then(res => {
        if (res.status === 200) {
          console.log("Success!")
          const token = res.data.token;
          window
            .localStorage
            .setItem('token', token)
          window
            .localStorage
            .setItem('user', JSON.stringify(res.data))
          window
            .localStorage
            .setItem('time', moment())
          window
            .localStorage
            .setItem('activeuser', JSON.stringify(res.data.user_user[0]))

          setTimeout(() => {
            history.push('/');
          }, 10);

        }

      })
      .catch((error) => {
        const res = error.response
        try {
          if (res.status === 400) {
            console.log("Log in failed")
            this.setState({animate: false, active: true, waitForResponse: false, errors: "Invalid credentials"})
          } else {
            console.log("Log in failed")
            this.setState({animate: false, active: true, waitForResponse: false, errors: "Problem logging in. Please try again"})
          }
        } catch (e) {
          this.setState({animate: false, active: true, waitForResponse: false, errors: "Problem logging in. Please try again"})

        }
        console.log("Error: ", error, "Res: ", error.response);

      })

  };

  handleBtnAnimate(e) {
    e.preventDefault();
    this.validateEmail(this.state.emailValue)
    this.validatePass(this.state.passwordValue)
    const formValid = !this.state.emailError && this.state.emailValue && !this.state.passError && this.state.passwordValue
    this.setState({formValid: formValid})
    if (!this.state.waitForResponse && formValid) {
      this.setState({animate: true, active: false, waitForResponse: true});
      const user = {
        username: this.state.emailValue,
        password: this.state.passwordValue
      };

      // const {login} = this.props.actions;
      this.login(user, this.props.history)
      // this.setState(stateRes)
    }

  }

  onCloseNotification = () => {
    this.setState({showNotificationBar: false})

  }

  onInputPass(target) {
    const name = target.name + "Value"

    this.setState({[name]: target.value})

    this.validatePass(target.value)
  }

  validatePass = (value) => {
    if (value === undefined || value === '') {
      this.setState({passError: "Please enter your password"})
    } else if (value) {
      const inputValid = value.length >= 6;
      const passError = inputValid
        ? ''
        : 'Password is too short';
      this.setState({passError: passError})
    }
  }

  onInputEmail(target) {
    const name = target.name + "Value"

    this.setState({[name]: target.value})

    this.validateEmail(target.value)
  }

  validateEmail = (value) => {
    if (value === undefined || value === '') {
      this.setState({emailError: "Please Enter Your Email"})
    } else if (value) {
      const inputValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
      var emailError = inputValid
        ? ''
        : 'Email is invalid';
      this.setState({emailError: emailError})
    }
  }

  componentDidMount = () => {
    const authenticated = localStorage.getItem('token')
      ? true
      : ''
    console.log(authenticated, 'Login authentication status')

    if (authenticated) {
      this
        .props
        .history
        .push('/')
    }

    const state = store.getState()
    const notificationBar = state.rootReducer.notificationBar
    const msg = notificationBar[0]
      ? notificationBar[0].message
      : ""
    // this.setState({showNotificationBar:true, notificationBarMsg: msg})
    this.setState({showNotificationBar: notificationBar[0], notificationBarMsg: msg})
    // store.dispatch( removeNotificationBar({}) )

  }

  render() {
    // const state = store.getState()
    const {classes} = this.props;

    console.log("Store state is ", this.state.showNotificationBar)
    let notificationBar;
    notificationBar = <div
      className={this.state.showNotificationBar
      ? 'notifications '
      : 'hidden'}>
      <div className="notification-bar-msg">{this.state.notificationBarMsg}</div>
      <div className='notifcations-close-icon' onClick={this.onCloseNotification}></div>

    </div>

    return (
      <Grid container spacing={0}>
        {notificationBar}
        <Grid className="" item xs={1} sm={2} lg={2} md={2}></Grid>
        <Grid className="" item xs={10} sm={8} lg={8} md={8}>
          <Grid container spacing={0}>
            <Grid className="" item xs={12} sm={12} md={12} lg={12}>
              <form method="post" className="landing-form">
                <Grid container spacing={0}>
                  <Grid className="" item xs={12} sm={12} lg={12} md={12}>
                    <div className="spekra-logo"><img
                      src="https://s3.amazonaws.com/-user-dashboard/images/white-logo.svg"
                      alt="Unemployed Tutor logo"/></div>
                  </Grid>

                  <Grid className="" item xs={12} sm={12} lg={12} md={12}>
                    <div className="title">LOG IN</div>
                  </Grid>

                  <br></br>
                  <br></br>
                  <Grid className="" item xs={12} sm={12} lg={12} md={12}>

                    <Input
                      type="email"
                      classes="icon-username"
                      label="Username"
                      name="email"
                      placeholder="Email Address"
                      error={this.state.emailError}
                      onChange={this.onInputEmail}
                      validate={false}/>

                  </Grid>
                  <Grid className="" item xs={12} sm={12} lg={12} md={12}>

                    <Input
                      error={this.state.passError}
                      type="password"
                      classes="icon-password"
                      label="Password"
                      name="password"
                      placeholder="Password"
                      onChange={this.onInputPass}
                      validate={false}/>
                  </Grid>
                  <Grid className="" item xs={12} sm={12} lg={12} md={12}>
                    <span className="errors">
                      <ValidationErrors
                        errors={[this.state.emailError, this.state.passError, this.state.errors]}/>
                    </span>
                  </Grid>

                  <Grid className="" item xs={12} sm={12} lg={12} md={12}>
                    <div className={classes.sectionDesktop}>
                      <div className="login-link " style={{ width: '100%'}}>
                        <span className="forgot-password">
                          <a href="/forgot-password">Forgot Your Password?</a>
                        </span>
                        <button
                          className="login-btn login-btn-desktop pull-right"
                          type="button"
                          id=""
                          onClick={this.handleBtnAnimate}>
                          {this.state.animate
                            ? <span
                                className={this.state.animate
                                ? "spinner"
                                : ""}></span>
                            : "LOG IN"}</button>
                      </div>
                    </div>
                    <div className={classes.sectionMobile}>
                      <div className=" " style={{ width: '100%'}}>
                        <div className="forgot-password">
                          <a href="/forgot-password">Forgot Your Password?</a>
                        </div>
                        <div style={{marginTop: '30px', width: '100%'}}>
                          <button
                            className="login-btn"
                            style={{width: '100%'}}
                            type="button"
                            id=""
                            onClick={this.handleBtnAnimate}>
                            {this.state.animate
                              ? <span
                                  className={this.state.animate
                                  ? "spinner"
                                  : ""}></span>
                              : "LOG IN"}</button>
                        </div>
                      </div>
                    </div>
                  </Grid>

                  <Grid className="" item xs={12} sm={12} lg={12} md={12}>

                    <hr className="dashboard-or"></hr>
                  </Grid>
                  {this.goToSignup()}
                  <Grid className="" item xs={12} sm={12} lg={12} md={12}>
                    <button onClick={this.setSignupRedirect} type="button" className="signup-btn">SIGN UP</button>
                  </Grid>
                </Grid>
              </form>

            </Grid>
          </Grid>
        </Grid>
        <Grid className="" item xs={1} sm={2} lg={2} md={2}></Grid>
        {/* <Grid className="" item xs={1}></Grid> */}
      </Grid>
    )
  }

}

const {object} = PropTypes;
LogIn.propTypes = {
  actions: object.isRequired
};
const mapDispatch = (dispatch) => {
  return {
    actions: bindActionCreators(sessionActions, dispatch)
  };
}

LogIn.propTypes = {
  classes: PropTypes.object.isRequired
};

export default connect(null, mapDispatch)(withStyles(styles)(withRouter(LogIn)))
