import React from 'react';
import DecreaseIcon from '../images/icons/Decrease.svg'
import IncreaseIcon from '../images/icons/Increase.svg'

class MoneyFlowPill extends React.Component {
  constructor() {
    super();
    this.state = {
      someKey: 'someValue'
    };
  }

  render() {
    return (
      <div className="row money-flow">
        <div className="col-xs-6 padding-16">
          <div className="row">
            <div className="col-xs-1">
              <img src={`${DecreaseIcon}`} alt=""></img>
            </div>
            <div className="col-xs-11 padding-left-8">
              <div className="text-red">{this.props.gain}</div>
              <div className="period">{this.props.gainPeriod}</div>
            </div>
          </div>
        </div>

        <div className="col-xs-6 border-left padding-16">
          <div className="col-xs-1">
            <img src={`${IncreaseIcon}`} alt=""></img>
          </div>
          <div className="col-xs-11 padding-left-8">
            <div className="text-green">{this.props.loss}</div>
            <div className="period">{this.props.lossPeriod}</div>
          </div>
        </div>

      </div>
    );
  }

  componentDidMount() {
    this.setState({someKey: 'otherValue'});
  }
}

export default MoneyFlowPill;
