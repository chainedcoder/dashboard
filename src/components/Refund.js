import React from 'react';
import SmartAvatar from './SmartAvatar';
import SimpleInput from '../components/SimpleInput'
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import {titleCase} from '../global/functions'

class Refund extends React.Component {
  constructor() {
    super();
    this.state = {
      open: false,
      refundData: {
        amount: '0',
        phone: '25470123456',
        name: 'John Doe'
      }

    };
  }
  closeRefund = () => {
    // this.setState({open: false})
    console.log(this.props)
    this.props.onClose()
  }

  render() {
    return (
      <div>
        <Dialog
          open={this.state.open}
          onClose={this.closeRefund}
          aria-labelledby="form-dialog-title"
          className="dialog">
          <div className="dialog-content">
            <div className="cashout-dialog-title">
              Issue Refund
            </div>
            <div className="dialog-body refund-dialog-body">
              <div className="refund-head">
                <div className="col-sm-2">
                  <SmartAvatar className="avatar-blue" names={this.state.refundData.name}></SmartAvatar>
                </div>
                <div
                  className="col-sm-10"
                  style={{
                  'paddingLeft': '4px'
                }}>
                  <div className='refund-customer-name'>
                    {titleCase(this.state.refundData.name)}
                  </div>
                  <div>
                    <div className="pull-left refund-customer-phone">
                      {this.state.refundData.phone}
                    </div>
                  </div>
                </div>
              </div>
              <hr />
              <div>
                <div className="refund-details">
                  <p className='refund-highlite'> {this.state.refundData.amount}</p>
                  <p>You are refunding a charge of {this.state.refundData.amount}</p>
                  <p>To refund a partial amount, enter it in the box below.</p>

                </div>

                <div className='row'>
                  <div className="col-sm-8">
                    <SimpleInput
                      id='amount'
                      name='amount'
                      type='text'
                      value={this.state.refundData.amount}
                      inputClass='refund-input'
                      placeholder=''/>
                  </div>
                  <div className='col-sm-4'>
                    <Button className="pull-left refund-btn">Refund</Button>
                  </div>
                </div>

              </div>
              <div></div>

            </div>
          </div>

        </Dialog>
      </div>
    );
  }

  componentDidMount() {
    this.setState({open: false});
  }
  componentDidUpdate = (prevProps, prevState) => {
    if (prevProps !== this.props) {
    const rData = this.props.refundData
      this.setState({open: this.props.open, refundData: rData});
    }
  }
}

export default Refund;
