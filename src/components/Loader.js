import React from 'react';
import Dialog from '@material-ui/core/Dialog';

class Loader extends React.Component {
  constructor() {
    super();
    this.state = {
      loading: false
    };
  }

  render() {
    return (
      <Dialog open={false} aria-labelledby="form-dialog-title" className="">
        <div className='loader'>
          <div className="spinner2"></div>
        </div>

      </Dialog>
    )

  }

  componentDidMount() {
    this.setState({loading: this.props.open});
  }
  
  componentDidUpdate = (prevProps, prevState) => {
    if (prevProps!==this.props){
        this.setState({loading: this.props.open});
    }
  }
  
}

export default Loader;
