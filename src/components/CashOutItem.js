import React from 'react';
import {withRouter} from 'react-router-dom'
import STimeAgo from '../components/STimeAgo'
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import moment from 'moment';
class CashOutItem extends React.Component {
  constructor() {
    super();

    this.handleTransRowClick = this
      .handleTransRowClick
      .bind(this)
  }

  handleTransRowClick = () => {
    console.log(this.props);
    this
      .props
      .history
      .push('/transactions/details/' + this.props.row.id)
  }

  render() {
    const row = this.props.row
    const rows = this.props.rows
    const date = moment(row.date, 'DD/MM/YYYY hh:mm:ss')
    const fDate = date.format('lll');
    const lastrow = <TableCell className="table-cell" numeric>{row.amount}
      <span className="pull-right">
        <div className="more-options"></div>
      </span>
    </TableCell>
    return (
      <TableRow className={row.id === rows.length
        ? "last-row"
        : ""}>
        <TableCell className="text-blue" component="th" scope="row">
          <span>{row.name}</span>
        </TableCell>
        <TableCell className="text-blue" numeric>{row.phone}</TableCell>
        <TableCell className="table-cell" numeric>
          <div className="tooltip">
            <span className="tooltiptext">{fDate}</span>
            <STimeAgo date={row.date}/>
          </div>
        </TableCell>
        {this.props.children || lastrow}
      </TableRow>
    )
  }

}

export default withRouter(CashOutItem);
