import React, { Component } from 'react';
class ValidationErrors extends Component {

  render() {
    var Errors = [];
    var errors = this.props.errors;
      for (var i = 0; i < errors.length; i++){
        if(errors[i]){
            Errors.push(<p key={i}> {errors[i]}</p>)
        } 
      }
      return (
        <div className='formErrors col-sm-12' > 
        {Errors}
        </div>
      );
    }
}

export default ValidationErrors;
