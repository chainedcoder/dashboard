import React from 'react';
import { withRouter } from 'react-router-dom';
import STimeAgo from '../components/STimeAgo';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import moment from 'moment';

class RowItem extends React.Component {
    constructor() {
        super();
        this.state = {
          optionsOpen: false,
        }

    
        this.handleTransRowClick =  this.handleTransRowClick.bind(this)
      }

  handleTransRowClick = () => {
    this.props
      .history
      .push('/orders/'+this.props.row.tid+'/')
  }

  handleRefund = event => {
    this.props.onRefund(this.props.row.amount, this.props.row.phone, this.props.row.name)
  }
  onCloseRefund = () => {
    this.setState({refundOpen:false})
  }

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  showOptions = event => {
    this.setState({
      optionsOpen: !this.state.optionsOpen,
      anchorEl: event.currentTarget,
    })
    
  }

  
  render() {
    const row = this.props.row
    const rows = this.props.rows
    var fDate = moment(this.props.date).format('lll');
    return (
      <TableRow
        className={row.id === rows.length
        ? "last-row"
        : ""}
        >
        <TableCell className="text-blue" component="th" scope="row">
          {/* <SmartAvatar className="avatar-blue" names={row.name}/> */}
          <span className="sentence-case">{row.subject}</span>
        </TableCell>
        <TableCell className="text-blue" numeric>{row.tid}</TableCell>
        <TableCell className="table-cell text-date-amount" numeric>
        <div className="tooltip">
                    <span className="tooltiptext">{fDate}</span>
                      <STimeAgo date={row.deadline}></STimeAgo>
                    </div>
        </TableCell>
        <TableCell className="table-cell text-date-amount"  numeric><div style={{display: 'inline-block', padding: '11px 0'}}>{row.amount}</div>
          <div  className="pull-right" onClick={this.showOptions} style={{display: 'inline-block', padding: '2px 0'}}>
          <div className="tooltip">
                    <span className="tooltiptext">More Actions</span>
            {/* <img src={`${MoreIcon}`} alt=""></img> */}
            <div className="more-options"></div>
            </div>
            <Menu className="option-menu" id="render-props-menu" anchorEl={this.state.anchorEl} open={this.state.optionsOpen} onClose={this.handleClose}>
              <MenuItem onClick={this.handleTransRowClick}>Payment Details</MenuItem>
              <MenuItem onClick={this.handleRefund}>Customer Profile</MenuItem>
              <MenuItem onClick={this.handleRefund}>Refund Payment</MenuItem>
            </Menu>
          </div>
        </TableCell>
      </TableRow>
    )
  }

}

export default withRouter(RowItem);
