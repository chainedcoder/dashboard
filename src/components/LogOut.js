import React , {Component} from 'react';
import {withRouter} from 'react-router-dom';
import store from '../redux/index'
import {showNotificationBar} from '../redux/actions.js'

class LogOut extends Component {
    render() {
        return(<div></div>)
    }
    
    componentDidMount = () => {
      
        // window.localStorage.removeItem('token')
        // window.localStorage.removeItem('user')
        // window.localStorage.removeItem('time')
    store.dispatch( showNotificationBar({ message: 'You Have Succesfully Logged Out!', timeout: 1 }) )

        this.props.history.push('/log-in');
    }
    
}


export default withRouter(LogOut)
  