import React, {Component} from 'react'
import Input from './Input'
import ValidationErrors from './ValidationErrors'
import API, {ENDPOINTS} from '../Api/Constants'
import {withRouter} from 'react-router'
import * as qp from '../actions/urlParams';

class SignUpConfirm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      success: false,
      formValid: false,
      animate: false,
      waitForResponse: false,
      passwordValue: '',
      cpasswordValue: '',
      passwordError: '',

    };
    this.handleBtnAnimate = this
      .handleBtnAnimate
      .bind(this);

  }
  

  setPassword = (password) => {
    // const paramString = this.props.location.search
    const params = qp.queryParams(this.props)

    API.post(ENDPOINTS.setPassword, JSON.stringify(password), {
      headers: {
        'Authorization': params.token
      }
    }).then(res => {

      if (res.status === 200) {
        this.setState({success:true})
      }

    }).catch((error) => {
      const res = error.response
      try {
        let err =""
        try {
          err = res.data.error !== undefined
            ? res.data.error
            : 'Oops, Something went wrong. Try again.'
        } catch (e) {
           err = 'Oops, Something went wrong. Try again.'

        }
        if (res.status === 400) {
          this.setState({animate: false, active: true, waitForResponse: false, errors: err})
        } else if (res.status === 403) {
          this.setState({animate: false, active: true, waitForResponse: false, errors: "Oops, You lack the authorization to perform this action"})
        } else {
          this.setState({animate: false, active: true, waitForResponse: false, errors: err})
        }
      } catch (e) {
        this.setState({animate: false, active: true, waitForResponse: false, errors: "Something isn't right. Check your connection and try again."})

      }
      console.log("Error: ", error, "Res: ", error.response);

    })

  };

  onInputPassword = (target) => {
    const name = target.name + "Value"
    this.setState({[name]: target.value})

    name === 'passwordValue'
      ? this.validatePassword(target.value)
      : this.validateCPassword(target.value)
  }

  validatePassword = (value) => {
    if (value.length < 6) {
      this.setState({passwordError: "Try a stronger password"})
      return false
    } else {
      this.setState({passwordError: ""})
      return true
    }
  }

  validateCPassword = (value) => {
    if (value.length < 6) {
      this.setState({cpasswordError: 'Password mismatch'})
      return false
    } else {
      this.setState({passwordError: ""})
      return true
    }
  }

  handleBtnAnimate(e) {
    e.preventDefault();
    const formValid = this.validatePassword(this.state.passwordValue) && this.validatePassword(this.state.cpasswordValue)

    this.setState({formValid: formValid})
    if (!this.state.waitForResponse && formValid) {
      this.setState({animate: true, active: false, waitForResponse: true});
      const password = {
        password: this.state.passwordValue
      };


      // const {login} = this.props.actions;
      this.setPassword(password)
      // this.setState(stateRes)
    }

  }
  render() {
    const setPassword = <div className="row">
    <div className="col-sm-offset-2">
      <form method="post" className="landing-form col-sm-10 ">
        <div className="row">
          <div className="spekra-logo"><img
            src="https://s3.amazonaws.com/dash-user-dashboard/images/white-logo.svg"
            alt="dashboard logo"/></div>
          <div className="title-text">Congratulations! Your account has been verified.</div>
          <br></br>
          <div className="title-text">Create a password to continue.</div>
          <br></br>
          <br></br>
          <Input
            type="password"
            classes=" icon-password"
            label="Password"
            name="password"
            onChange={this.onInputPassword}
            placeholder="Password"/>
          <Input
            type="password"
            classes=" icon-password"
            label="Confirm Password"
            onChange={this.onInputPassword}
            name="cpassword"
            placeholder="Confirm Password"/>
          <ValidationErrors errors={[this.state.passwordError, this.state.errors]}/>

          <button
            className="signup-purple-btn col-sm-12"
            type="button"
            id=""
            onClick={this.handleBtnAnimate}>
            {this.state.animate
              ? <span
                  className={this.state.animate
                  ? "spinner"
                  : ""}></span>
              : "CONTINUE"}</button>
        </div>
      </form>

    </div>
  </div>

  const success = <div className="row">
  <div className="col-sm-offset-2">
    <form method="post" className="landing-form col-sm-10 ">
      <div className="row">
        <div className="spekra-logo"><img
          src="https://s3.amazonaws.com/dash-user-dashboard/images/white-logo.svg"
          alt="dashboard logo"/></div>
        <div className="title-text">Password successfully created.</div>
        <br></br>
        <div className="title-text">Log into your account now.</div>
        <br></br>

        <a href='/log-in'>
        <button
        type='button'
          className="signup-purple-btn col-sm-12 "
          text="LOG IN"
          name=""
          id="">LOG IN</button></a>

      </div>
    </form>

  </div>
</div>
    return (
      <div>
        {this.state.success? success: setPassword}
      </div>
    );
  }
}

export default withRouter(SignUpConfirm);