import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import FilterIcon from '../images/icons/Filter.svg'
import {withRouter} from 'react-router'
import RowItem from './RowItem';
import DateRangePicker from './DateRangePicker'
import FilterButton from './FilterButton'
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import API, {ENDPOINTS} from '../Api/Constants'
import moment from 'moment';
import {queryParamsFromLink} from '../actions/urlParams'
import LoadMore from '../images/icons/LoadMoreIcon.svg'
import Loader from './Loader';
import Refund from './Refund'
import {titleCase} from '../global/functions'
import DatePicker from './DatePicker/DatePicker'
import request,{extend} from 'umi-request'


class TransactionsTable extends React.Component {
  constructor() {
    super();
    this.state = {
      filterOpen: false,
      selected: {
        SUCCESSFUL: 'SUCCESSFUL'
      },
      transParams: {
        status: "SUCCESSFUL",
        stype: 'C2M'
      },
      rows: [
        // {
        //   "tid": "SKE-C2M-0T",
        //   "stype": "C2M",
        //   "created": "2018-10-05T12:18:15.473044",
        //   "reference": "some-id-here",
        //   "phone": 254797561788,
        //   "person_name": "Sarah Loise",
        //   "status": "SUCCESSFUL",
        //   "provider": 1,
        //   "user": 2,
        //   "person": 2,
        //   "m_request_amount": "1.0000",
        //   "p_request_amount": "0.0000",
        //   "m_paid_amount": "1.0150",
        //   "p_paid_amount": "0.0000",
        //   "s_paid_amount": "0.0101",
        //   "m_amount": 0.0,
        //   "s_amount": 0.0,
        //   "p_amount": 0.0
        // }
      ],
      loading: false,
      showRefund: false,
      refundData: {
        amount: '0',
        phone: '25470123456',
        name: 'John Doe'
      }
    };

    
  }

  
  handleClickAwayFilter = () => {
    this.setState({filterOpen: false});
  }

  handleFilterOpen = () => {
    this.setState({
      filterOpen: !this.state.filterOpen
    });
  }

  onChecked = (name, checked) => {
    let newSelected = {}
    newSelected[name] = checked
    let params = this.state.transParams
    params.status = name
    this.setState({selected: newSelected, transParams: params})
    this.handleFetchTransactions(true)
    
  }

  filterByDate = (from, to) => {
    let transParams = this.state.transParams
    transParams.from_date = moment(from)
      .add(1, 'days')
      .format('YYYY-MM-DD')
    transParams.to_date = moment(to)
      .add(1, 'days')
      .format('YYYY-MM-DD')
    // let params = this.state.transParams

    this.setState({transParams: transParams})
    this.handleFetchTransactions(true)
  }

  handleFetchTransactions = (method) => {
    // const request = extend ({
    //   prefix: '127.0.0.1:/8000/api/v1'
    // })
    console.log("Fetching orders: ", ENDPOINTS.orders)
    this.setState({loading: true})
    // , { method:'post', data: 'some data', headers: { 'Content-Type': 'multipart/form-data'} }
    request
      .get('http://127.0.0.1:8000/api/v1/orders/all/')
      .then(res => {
        console.log(res)
        if (res) {
          const cRows = this.state.rows
          let trans = []
          if (method) {
            trans  = res          
          } else {
            trans = [
              ...cRows,
              ...res
            ]
            console.log(" Transactions=> ", trans)
          }
          // let nextParams = queryParamsFromLink(res.data.next)
          // let transParams = this.state.transParams
          // transParams.offset = nextParams.offset
          // transParams.limit = nextParams.limit

          this.setState({loading: false, rows: trans })
        }
        this.setState({loading: false})

      })
      .catch((error) => {
        if (error.code === 'ECONNABORTED') {
          console.log('Request canceled', error.message);
          console.log('Retrying.....');
          this.handleFetchTransactions(false)
        } 

      })
  }

  loadMore = () => {
    if (!this.state.loading) {
      this.setState({loading: true})
      this.handleFetchTransactions(false)
    }

  }

  handleRefundClicked = (amt, phone, name) => {
    const refundData = {
      phone: phone,
      name: name,
      amount: amt
    }
    console.log("REfund data ", refundData)
    this.setState({showRefund: true, refundData: refundData})
  }

  handleCloseRefund = () => {
    this.setState({showRefund: false})
  }

  componentDidMount() {
    this.handleFetchTransactions(true)
  }

  render() {

    let id = 0;
    function createData(tid, subject, client, deadline, amount) {
      id += 1;
      return {
        id,
        tid,
        subject,
        client,
        deadline,
        amount,
      };
    }

    let rows = []
    const trans = this.state.rows
    for (let i = 0; i < trans.length; i++) {
      const subject = titleCase(trans[i].subject.toLowerCase());
      const tid = trans[i].order_number.toUpperCase()
      const deadline = trans[i].deadline
      var fDate = moment(deadline).format('lll');

      const client = trans[i].user || "John Doe"

      const currency = "$ " //window
      //   .localStorage
      //   .getItem('currency')

      // if (this.state.transParams.status === 'SUCCESSFUL') {
        const amount = (parseFloat(trans[i].total_price * 100) / 100).toLocaleString()
        const row = createData(tid, subject, client, fDate, currency + amount)
       console.log("the row is ", row)
        rows.push(row)
      // } else {
      //   const amount = (parseFloat(trans[i].m_request_amount * 100) / 100).toLocaleString()
      //   const row = createData(tid, fullName, phone, fDate, currency + " " + amount)
      //   rows.push(row)
      // }

    }

    let filterId = 0
    const createFilter = (label, value) => {
      filterId += 1;
      return {filterId, label, value}

    }

    const filterItems = [
      createFilter('Successful', "SUCCESSFUL"),
      createFilter('Failed', "FAILED"),
      createFilter('Pending', "PENDING")
    ]

    return (
      <div>
        <Loader open={this.state.loading}/>
        <Refund
          open={this.state.showRefund}
          onClose={this.handleCloseRefund}
          refundData={this.state.refundData}/>

        <div className="table card">
          <div className="data-table-title row">

            <ClickAwayListener onClickAway={this.handleClickAwayFilter}>
              <div className="pull-left filter-btn">
                <div
                  className="card-button pull-left margin-left-20 card-button-transactions"
                  onClick={this.handleFilterOpen}>

                  <img className="filtericon" src={`${FilterIcon}`} alt="filter icon"></img>
                  Filter
                </div>
                {this.state.filterOpen
                  ? <FilterButton
                      filterItems={filterItems}
                      selected={this.state.selected}
                      onChecked={this.onChecked}/>
                  : ""}

              </div>
            </ClickAwayListener>
            <DatePicker onChange={this.filterByDate}/> {/* <div className="pull-right">

              <DateRangePicker onChange={this.filterByDate}/>
            </div> */}

          </div>
          <Table className={"data-table"}>
            <TableHead>
              <TableRow className="head-row transactions-head-row">
                <TableCell>Title</TableCell>
                <TableCell numeric>Order Number</TableCell>
                <TableCell numeric>Deadline</TableCell>
                <TableCell numeric>Amount</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map(row => {
                return (<RowItem
                  onRefund={(amount, tid, client) => {
                  this.handleRefundClicked(row.amount, row.client, row.client)
                }}
                  key={row.id}
                  row={row}
                  rows={rows}
                  parent={this}/>);
              })}
            </TableBody>
          </Table>
        </div>
        {this.state.more
          ? <div className="load-more" onClick={this.loadMore}>Load More
              <span className="load-arrow">
                <img className="down-arrow" src={`${LoadMore}`} alt="load more icon"></img>
              </span>
            </div>
          : ''}

      </div>

    );
  }

}

export default withRouter(TransactionsTable);
