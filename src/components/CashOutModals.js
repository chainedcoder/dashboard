import React from 'react';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Input from '@material-ui/core/Input';
import SmartAvatar from '../components/SmartAvatar';
import SimpleInput from '../components/SimpleInput';
import CheckoutDialog from '../components/CheckoutDialog';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import PlusIcon from '../images/icons/Plus.svg';

class CashOutModals extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cashOutConfirmOpen: false,
      cashOutFormOpen: true,
      cashoutMethod: "",
      amountFormOpen: false,
      recentBank: "dtbBank",
      FormFields: ""
    };
    this.handleConfirmOpen = this
      .handleConfirmOpen
      .bind(this)
    this.handleConfirmClose = this
      .handleConfirmClose
      .bind(this)
    this.handleFormCashout = this
      .handleFormCashout
      .bind(this)
    this.handleCashoutChange = this
      .handleCashoutChange
      .bind(this)
    this.handleAmountForm = this
      .handleAmountForm
      .bind(this)
    this.handleRecentBankChange = this
      .handleRecentBankChange
      .bind(this)
  }

  handleCashoutChange = event => {

    this.setState({cashoutMethod: event.target.value});
  };

  handleConfirmOpen = () => {
    this.setState({cashOutConfirmOpen: true, amountFormOpen: false});
  };

  handleConfirmClose = () => {
    this
      .props
      .onClose()
    this.setState({cashOutConfirmOpen: false});
  };

  handleAmountFormClose = () => {
    this
      .props
      .onClose()
    this.setState({amountFormOpen: false});
  };

  handleFormCashout = () => {
    this.setState({cashOutFormOpen: true, amountFormOpen: false, cashOutConfirmOpen: false});
  }

  handleFormCashoutClose = () => {
    this
      .props
      .onClose()
    this.setState({amountFormOpen: false});
  }

  handleAmountForm = () => {
    this.setState({amountFormOpen: true, cashOutFormOpen: false});
  }

  handleRecentBankChange = event => {
    this.setState({recentBank: event.target.value});

  };

  render() {

    const BankAccountFields = <div className="col-sm-12">
      <SimpleInput
        className="dialog-enter-details"
        id="bankaccno"
        label="Enter your Bank Account Number"
        type="numerical"
        InputClass=""
        name="bankaccno"
        placeholder="1234567890123"/>
      <SimpleInput
        className="dialog-enter-details"
        id="bankname"
        label="Enter Your Bank's Name"
        type="text"
        InputClass=""
        name="bankname"
        placeholder="Standard Chartered Bank"/>
      <SimpleInput
        className="dialog-enter-details"
        id="bankbranch"
        label="Enter Your Bank's Branch"
        type="text"
        InputClass=""
        name="bankbranch"
        placeholder="Highways Branch"/>
    </div>

    const PaypalFields = <div className="col-sm-12">
      <SimpleInput
        className="dialog-enter-details"
        id="paypal"
        label="Enter Your Paypal ID"
        type="numerical"
        name="paypal"
        InputClass=""
        placeholder="123456"/>
    </div>

    // const TillFields = <div className="col-sm-12"><SimpleInput
    //   className="dialog-enter-details"
    //   id="tillnumber"
    //   label="Enter Your Buy Goods Till Number"
    //   type="numerical"
    //   name="tillnumber"
    //   InputClass=""
    //   placeholder="123456"/></div>

    // const PhoneFields = <div className="col-sm-12">
    //   <SimpleInput
    //     className="dialog-enter-details"
    //     id="phonenumber"
    //     label="Enter Your Phone Number"
    //     type="numerical"
    //     name="phonenumber"
    //     InputClass=""
    //     placeholder="254720123456"/>
    // </div>

    const SelectedForm = () => {
      switch (this.state.cashoutMethod) {
        case 'bankaccnumber':
          return BankAccountFields;
        case 'paypal':
          return PaypalFields
        // case 'tillnumber':
        //   return TillFields
        // case 'phonenumber':
        //   return PhoneFields
        default:
          return "";
      }
    }

    const currency = window.localStorage.getItem('currency')
    return (
      <div>
        {/* Cashout dialogue to confirm password. */}
        <CheckoutDialog open={this.state.cashOutConfirmOpen}>
          <div className="cashout-center">
            <SmartAvatar names="Brenda Wambui" className="dialog-avatar-cashout"></SmartAvatar>
            <div>Please confirm your password to cash out</div>
          </div>
          <div>
            <SimpleInput
              className="dialog-password"
              id="password"
              label="Enter your password"
              type="password"
              iconClass="dark-pass-vision"
              name="password"
              placeholder="Password"/>
          </div>

          <span className="forgot-password-cashout">Forgot your password?</span>
          <br></br>
          <div className="dialog-btn-sec">
            <Button
              className="blue-button dialog-btn bg-white text-paleblue pull-left margin-zero"
              onClick={this.handleConfirmClose}>Cancel</Button>
            <Button className="blue-button dialog-btn pull-right text-white-button">Cash Out</Button>

          </div>
        </CheckoutDialog>

        {/* Dialog Checkout form  */}
        <CheckoutDialog open={this.state.cashOutFormOpen}>
          <div
            className="select-wrapper col-sm-12 dialog-password"
            style={{
            marginTop: -30
          }}>
            <label htmlFor="cashoutmethod" className="cashout-label">Select Cashout Method</label>
            <Select
              value={this.state.cashoutMethod}
              onChange={this.handleCashoutChange}
              displayEmpty
              name="cashout"
              className="simple-select"
              input={< Input name = "name" id = "name-helper" />}>

              <MenuItem value="bankaccnumber">Bank Account</MenuItem>
              <MenuItem value="paypal">Paypal Number</MenuItem>
              {/* <MenuItem value="tillnumber">Till Number</MenuItem>
              <MenuItem value="phonenumber">Phone Number</MenuItem> */}
            </Select>

          </div>
          {SelectedForm()}

          <div className="dialog-btn-sec col-sm-12">
            <Button
              className="blue-button dialog-btn bg-white text-paleblue pull-left margin-zero"
              onClick={this.handleFormCashoutClose}>Cancel</Button>
            <Button
              className="blue-button dialog-btn pull-right text-white-button"
              onClick={this.handleAmountForm}>Continue</Button>

          </div>

        </CheckoutDialog>

        {/* Cashout Form for entering amount */}

        <CheckoutDialog open={this.state.amountFormOpen}>

          <div>
            <SimpleInput
              className="dialog-enter-details"
              id="amount"
              label="Enter Amount"
              type="text"
              InputClass=""
              name="amount"
              placeholder={currency + " 0.00"}
              style={{
              marginTop: '-40px'
            }}/>
          </div>
          <div className="add-acc col-sm-12 dialog-enter-details">
            <div
              className="col-sm-10"
              style={{
              padding: "10px 0"
            }}>Recent Bank Accounts</div>

            <div className="col-sm-2" onClick={this.handleFormCashout}>
              <div className="tooltip">
                <span className="tooltiptext">new payment method</span>
                <img src={`${PlusIcon}`} alt="add icon" className="pull-right plus-card"></img>
              </div>
            </div>
          </div>

          <div className="recent-bank-radio col-sm-12">

            <RadioGroup
              aria-label="Recent Bank Accounts"
              name="recentBank"
              onClick={this.handleRecentBankChange}
              className="radio"
              value={this.state.recentBank}>
              <FormControlLabel
                value="bankaccno"
                control={< Radio color = "primary" />}
                label="Bank Account (01234567890123)"/>
              {/* <FormControlLabel
                value="phonenumber"
                control={< Radio color = "primary" />}
                label="Phone Number (254720123456)"/> */}
              <FormControlLabel
                value="paypal"
                control={< Radio color = "primary" />}
                label="Paypal ID (exampleuser@kmail.com)"/>
              {/* <FormControlLabel
                value="tillnumber"
                control={< Radio color = "primary" />}
                label="Till Number (123456)"/> */}

            </RadioGroup>
          </div>

          <div className="dialog-btn-sec col-sm-12">
            <Button
              className="blue-button dialog-btn bg-white text-paleblue pull-left margin-zero"
              onClick={this.handleAmountFormClose}>Cancel</Button>
            <Button
              className="blue-button dialog-btn pull-right text-white-button"
              onClick={this.handleConfirmOpen}>Cash Out</Button>

          </div>
        </CheckoutDialog>
      </div>

    );
  }

  componentWillReceiveProps() {
    this.setState({cashOutFormOpen: this.props.open});
    if (typeof this.props.new !== "undefined") {
      if (this.props.new === false) {
        this.setState({amountFormOpen: true, cashOutFormOpen: false});
      }
      console.log("State in nt undf: ", this.state)

    }

  }

  componentWillMount() {
    this.setState({cashOutFormOpen: this.props.open});
    if (typeof this.props.new !== "undefined") {
      if (this.props.new === false) {
        this.setState({amountFormOpen: true, cashOutFormOpen: false});
      }
      console.log("State in nt undf: ", this.state)

    }
  }
}

export default CashOutModals;
