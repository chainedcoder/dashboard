import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { polyfill } from 'react-lifecycles-compat';
import makeEventProps from 'make-event-props';
import mergeClassNames from 'merge-class-names';
import detectElementOverflow from 'detect-element-overflow';

import Calendar from './cust-calendar/entry.nostyle';

import moment from 'moment';
// import ClickAwayListener from '@material-ui/core/ClickAwayListener';
// import DateFormatInput from './DateTimePicker/datepicker'
import CalendarIcon from '../../images/icons/Calender.svg'
import RightArrow from '../../images/icons/CalenderTo.svg'

const baseClassName = 'spk-date-picker';

export default class DatePicker extends PureComponent {
  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.isOpen !== prevState.isOpenProps) {
      return {
        isOpen: nextProps.isOpen,
        isOpenProps: nextProps.isOpen,
      };
    }

    return null;
  }
  constructor() {
    super();
    this.state = {
      datePickerToOpen: false,
      datePickerFromOpen: false,
      dateTo: moment().format('L'),
      dateFrom: moment().subtract(1, 'month').format('L'),
      min: moment().subtract(1, "years").format('L'),
      max: moment().toDate(),
      from: true,
      
    };

    this.calendarAnchor = React.createRef()
  }


  state = {};

  get eventProps() {
    return makeEventProps(this.props);
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.onClick);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.onClick);
  }

  onClick = (event) => {
    if (this.wrapper && !this.wrapper.contains(event.target)) {
      this.closeCalendar();
    }
  }

  openCalendar = () => {
    this.setState({ isOpen: true });
  }

  closeCalendar = () => {
    this.setState((prevState) => {
      if (!prevState.isOpen) {
        return null;
      }

      return { isOpen: false };
    });
  }

  toggleCalendarFrom = () => {
    this.setState({ isOpen: true, from:true });
  }

  toggleCalendarTo = () => {
    this.setState({ isOpen: true, from:false });
  }

  onFocus = (event) => {
    const { disabled, onFocus } = this.props;

    if (onFocus) {
      onFocus(event);
    }

    // Internet Explorer still fires onFocus on disabled elements
    if (disabled) {
      return;
    }

    this.openCalendar();
  }

  
  onChangeTo = (date) => {
    this.setState({
      dateTo: moment(date).format('L'),
      isOpen: false,

    })
    console.log("Date from and to ", moment(this.state.dateFrom))

    try {
      this.props.onChange(moment(this.state.dateFrom), moment(this.state.dateTo))

    } catch(e){
      console.log("Provide an onchange to return the date from clendar date picker.")
    }
  }

  onChangeFrom = (date) => {
    this.setState({
      dateFrom: moment(date).format('L'),
      from: false,
    })
    
  }

  handleDateTo(date) {
    console.log(this.calendarAnchor, " Calendar anchor")
    this.setState({
      from:false
    })
  }


  handleCloseCalendar(){
      this.setState({datePickerFromOpen:false, datePickerToOpen:false})
  }

  stopPropagation = event => event.stopPropagation();

  clear = () => this.onChange(null);

  renderInputs() {
    const {
      calendarIcon,
      clearIcon,
      disabled,
      locale,
      maxDate,
      maxDetail,
      minDate,
      name,
      returnValue,
      required,
      showLeadingZeros,
      value,
    } = this.props;
    const { isOpen } = this.state;

    const [valueFrom] = [].concat(value);

    return (
      <div className={`${baseClassName}__wrapper`}>
        
        {calendarIcon !== null && (
          <div className="pull-right">
          <div className="card-button pull-right date-box card-button-transactions">
            <div
              ref ={this.calendarAnchor}
              onClick={this.toggleCalendarFrom}
              onFocus={this.stopPropagation}
              onBlur={this.resetValue}
              style={{
              display: "inline-flex"
            }}>
              <div style={{
                display: "inline-flex"
              }}>
                <img className="calendar-icon " src={`${CalendarIcon}`} alt="calendar icon"></img>
              </div>
              <div className=" date">{this.state.dateFrom}</div>
            </div>
  
            <img className="right-arrow" src={`${RightArrow}`} alt="right arrow icon"></img>
  
            <div
              onClick={this.toggleCalendarTo}
              onFocus={this.stopPropagation}
              onBlur={this.resetValue}
              style={{
              display: "inline-flex"
            }}>
              <div style={{
                display: "inline-flex"
              }}>
                <img className="calendar-icon " src={`${CalendarIcon}`} alt="calendar icon"></img>
              </div>
              <div className=" date">{this.state.dateTo}</div>
            </div>
  
          </div>
          
        </div>
        )}
      </div>
    );
  }

  renderCalendar() {
    const { isOpen } = this.state;

    if (isOpen === null) {
      return null;
    }

    const {
      calendarClassName,
      className: datePickerClassName, // Unused, here to exclude it from calendarProps
      onChange,
      value,
      ...calendarProps
    } = this.props;

    const fromValue= moment(this.state.dateFrom).toDate()
    const toValue = moment(this.state.dateTo).toDate()
    const className = `${baseClassName}__calendar`;

    return (
      <div
        className={mergeClassNames(
          className, `${this.state.from? ' calendar-root-from ': ' calendar-root-to '}`,
          `${className}--${isOpen ? 'open' : 'closed'}`,
        )}
        ref={(ref) => {
          if (!ref || !isOpen) {
            return;
          }

          ref.classList.remove(`${className}--above-label`);

          const collisions = detectElementOverflow(ref, document.body);

          if (collisions.collidedBottom) {
            const overflowTopAfterChange = (
              collisions.overflowTop + ref.clientHeight + this.wrapper.clientHeight
            );

            // If it's going to make situation any better, display the calendar above the input
            if (overflowTopAfterChange < collisions.overflowBottom) {
              ref.classList.add(`${className}--above-label`);
            }
          }
        }}
      >
        {this.state.from ?
        <Calendar
        className={calendarClassName}
        onChange={this.onChangeFrom}
        value={fromValue || null}
        {...calendarProps}
      />:
      <Calendar
          className={calendarClassName}
          onChange={this.onChangeTo}
          value={toValue || null}
          {...calendarProps}
        />
        }
      </div>
    );
  }

  render() {
    const { className, disabled } = this.props;
    const { isOpen } = this.state;

    
    return (
      <div className='pull-right'>
      
      <div
        className={mergeClassNames(
          baseClassName,
          `${baseClassName}--${isOpen ? 'open' : 'closed'}`,
          `${baseClassName}--${disabled ? 'disabled' : 'enabled'}`,
          className,
        )}
        {...this.eventProps}
        onFocus={this.onFocus}
        ref={(ref) => {
          if (!ref) {
            return;
          }

          this.wrapper = ref;
        }}
      >
        {this.renderInputs()}
        {this.renderCalendar()}
      </div>
      </div>
    );
  }
}



const ClearIcon = (
  <svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 19 19">
    <g stroke="black" strokeWidth="2">
      <line x1="4" y1="4" x2="15" y2="15" />
      <line x1="15" y1="4" x2="4" y2="15" />
    </g>
  </svg>
);

DatePicker.defaultProps = {
  // calendarIcon: CalendarIcon,
  clearIcon: ClearIcon,
  isOpen: null,
  returnValue: 'start',
};

DatePicker.propTypes = {
  ...Calendar.propTypes,
  calendarClassName: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string),
  ]),
  calendarIcon: PropTypes.node,
  className: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string),
  ]),
  clearIcon: PropTypes.node,
  disabled: PropTypes.bool,
  isOpen: PropTypes.bool,
  name: PropTypes.string,
  returnValue: PropTypes.oneOf(['start', 'end', 'range']),
  required: PropTypes.bool,
  showLeadingZeros: PropTypes.bool,
};

polyfill(DatePicker);
