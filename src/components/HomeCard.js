import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

class HomeCard extends React.Component {
  constructor() {
    super();
    this.state = {
      someKey: 'someValue'
    };
  }

  render() {
    let rootClass = this.props.rootClass ? this.props.rootClass :''
    return (
      <Card className={"card "+ rootClass}>
        <CardContent className={"card-content summary-card " +this.props.className}>
          <div className="card-title">{this.props.title}</div>
          <div className="card-body">
            {this.props.children}
          </div>

        </CardContent>
      </Card>
    );
  }

  componentDidMount() {
    this.setState({someKey: 'otherValue'});
  }
}

export default HomeCard;
