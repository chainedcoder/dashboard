import React from 'react';
import moment from 'moment'
class STimeAgo extends React.Component {

  render() {    
    let dateT  = moment(this.props.date);

    if (!dateT.isValid()){
      dateT = moment(this.props.date,"DD/MM/YYYY HH:mm:ss")
    }
    
    const timeAgo= dateT.fromNow();
    return (
      <div>
          {timeAgo}
        
      </div>

    );
  }

}

export default STimeAgo;