
import React from 'react';
// import SuccessIcon from '../images/success.svg'
// import FailIcon from '../images/fail.svg'
class Button extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            animate: false,
            active:true,
            waitForResponse: false,
            submit: this.props.animate,
            fields: null
        };
        this.handleAnimate = this.handleAnimate.bind(this);
    }
    
    handleAnimate(e) {
        e.preventDefault();
        
        console.log(this.state)
        if (this.state.active && this.state.submit) {
            this.setState({
                animate: true,
                active: false
              });
        }
      
    }


    componentWillUpdate (props) {

    };

    componentWillReceiveProps (){
        console.log(this.props.animate)
        this.setState({
            animate: this.props.animate,
        })
    }

    render(){
        if(this.state.submit){
            console.log(this.props, "submitting")
        }
        return (
                <button
                    valid={this.state.formValid}
                    className={this.props.className} 
                    style={this.props.style} 
                    name={this.props.name} 
                    type={this.props.type}
                    id={this.props.id}
                    onClick={this.props.animate?this.handleAnimate:""}
                >  {this.state.animate?<span className={this.state.animate?"spinner":""}> </span>:this.props.text}</button>
        )
    }
}

export default Button;