import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import UploadIcon from '../images/icons/UploadIcon.svg'

class FileUpload extends React.Component {
  constructor() {
    super();
    this.state = {
      file: ''
    };

    this.dropZone = React.createRef()

  }

  onSave = () => {}

  onDrop = (ev) => {
    console.log('File dropped')

    ev.preventDefault();

    if (ev.dataTransfer.items) {
      for (var i = 0; i < ev.dataTransfer.items.length; i++) {
        if (ev.dataTransfer.items[i].kind === 'file') {
          var file = ev
            .dataTransfer
            .items[i]
            .getAsFile();
          console.log('... file[' + i + '].name = ' + file.name);
        }
      }
    } else {
      for (var i = 0; i < ev.dataTransfer.files.length; i++) {
        console.log('... file[' + i + '].name = ' + ev.dataTransfer.files[i].name);
      }
    }

    this.removeDragData(ev)
  }

  removeDragData = (ev) => {
    console.log('Removing drag data');

    if (ev.dataTransfer.items) {
      ev
        .dataTransfer
        .items
        .clear();
    } else {
      ev
        .dataTransfer
        .clearData();
    }
  }

  preventDefaults = (e) => {
    e.stopPropagation();
    e.preventDefault();
    return false;
  }

  handleClose = () => {
    this
      .props
      .onClose()
  }
  render() {
    if (this.dropZone.current) {
      console.log(this.dropZone.current)

    }
    // this.dropZone.addEventListener('drop', this.onDrop, false)
    return (
      <Dialog
        open={this.props.open}
        onClose
        ={this.handleClose}
        aria-labelledby="form-dialog-title"
        className="dialog">
        <div className="contact-us-content">
          <div className="cashout-dialog-title">
            {this.props.title === "undefined"
              ? 'Upload Image'
              : 'Upload Image'}
          </div>

          <div className='file-upload'>
            <div
              id='dragZone'
              onDrop={this.onDrop}
              onDrag={this.preventDefaults}
              ref={this.dropZone}
              className='drag-zone'>
              <div className='upload-icon'>
                <img src={`${UploadIcon}`}></img>
              </div>
              <div className='upload-info'>
                <div className='upload-highlite'>
                  Drag and drop to upload</div>

                <div className='upload-text'>
                <input type="file" id="imgupload" style={{'display':'none'}}/>

                  or <label className='text-blue' htmlFor='imgupload'> browse</label> to choose a file</div>
              </div>
            </div>
            <div className='upload-save'>
              <Button
                onClick={this.onSave}
                className="col-sm-12 contactus-btn contactus-btn-msg">Upload</Button>
            </div>

          </div>

        </div>

      </Dialog>

    )
  }

  componentDidMount() {
    this.setState({file: ''});
    setTimeout(() => {
      console.log(this.dropZone)
      try{

        this
          .dropZone.current
          .addEventListener('drop', this.onDrop, false)
      } catch(e) {
        
      }
    }, 1000);
    
    ['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
      window.addEventListener(eventName, this.preventDefaults, false)
    })
  }

  componentDidUpdate = (prevProps, prevState) => {
    if (prevProps != this.props) {

    }
  }

  componentWillUnmount = () => {
    ['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
      window.addEventListener(this.onDrop, this.preventDefaults, false)
    })

  }

}

export default FileUpload;
