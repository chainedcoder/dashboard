import React, { Component } from 'react';
class FormErrors extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formErrors: {},
     
    };
}

  componentWillReceiveProps(){
    this.setState({formErrors: this.props.formErrors})
  }
  render() {
    console.log(this.state.formErrors, "Form Errors")
    var Errors = [];
    var formErrors = this.state.formErrors;
      for (var fieldName in formErrors){
        if(formErrors[fieldName].length > 0){
            Errors.push(<p key={fieldName}>{fieldName} {formErrors[fieldName]}</p>)
        } 
      }
      return (
        <div className='formErrors col-sm-12' > 
        {Errors}
        </div>
      );
    }
}

export default FormErrors;
