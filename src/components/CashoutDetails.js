import React from 'react';
import BaseDashboard from '../components/BaseDashboard';
import Button from '@material-ui/core/Button';
import BackIcon from '../images/icons/Back.svg';
import {withRouter} from 'react-router-dom';

class CashoutDetails extends React.Component {
  constructor() {
    super();
    this.state = { someKey: 'someValue' };
  }

  render() {
    const currency = window.localStorage.getItem('currency')
      

    return (
        <BaseDashboard>
        <div  >
          <span className="back-button" onClick={this.props.history.goBack}><img
            src={`${BackIcon}`}
            style={{
            'width': 12 + 'px',
            'marginRight': '5px'
          }}
            alt="Back navigation icon"></img>
          Back
          </span>
        </div>
        <div className="padded-card trans-detail">
          <div className="trans-head">
            <div className="col-sm-1">
              
            </div>
            <div className="col-sm-11" style={{'paddingLeft': '4px'}}>
              <div>
                Amount Paid
              </div>
              <div>
                <div className="pull-left">
                  <h1>
                  {currency} 13,343.45
                  </h1>
                </div>
                <Button className="blue-button">Refund Payment</Button>
              </div>
            </div> 
          </div>
          <div >
            <div className="tran-card-title">PAYMENT DETAILS</div>
            <div className="padded-card">
              <table className="trans-table">
                <tbody>
                  <tr>
                    <td>
                      Amount paid:
                    </td>
                    <td className="caps">
                    {currency} 13,343.45
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Transaction Fee:
                    </td>
                    <td className="caps">
                    {currency} 23.00
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Date/Time:
                    </td>
                    <td>
                      2018/08/21 18:12:42
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Payment Status:
                    </td>
                    <td className="text-green caps">
                      Paid
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Description:
                    </td>
                    <td className="caps">
                      shelter
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Transaction ID:
                    </td>
                    <td className="caps">
                      UT-R2B-OLASD11
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div className="customer-detail">
            <div className="tran-card-title ">CUSTOMER DETAILS</div>
            <div className="padded-card">
              <table className="trans-table">
                <tbody>
                  <tr>
                    <td >
                      Name:
                    </td>
                    <td className="text-blue ">
                      John Doe
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Phone Number:
                    </td>
                    <td>
                      +254726788923
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Location:
                    </td>
                    <td>
                      Kileleshwa, Nairobi
                    </td>
                  </tr>
                </tbody>
              </table>

            </div>
          </div>
        </div>

      </BaseDashboard>
    );
  }

  componentDidMount() {
  }
}

export default withRouter(CashoutDetails);
