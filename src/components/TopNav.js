import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import Badge from '@material-ui/core/Badge';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import {withStyles} from '@material-ui/core/styles';
import SmartAvatar from './SmartAvatar';
import blue from '@material-ui/core/colors/blue';
import deepPurple from '@material-ui/core/colors/deepPurple';
import { Link } from 'react-router-dom'
import {withRouter} from 'react-router-dom';
import { connect } from 'react-redux';
import {titleCase} from '../global/functions'
import MenuIcon from '@material-ui/icons/Menu';
// import classNames from 'classnames';



const styles = theme => ({
  root: {
    width: '100%'
  },
  nav: {
    zIndex: 999
  },
  grow: {
    flexGrow: 1
  },

  title: {
    display: 'none',
    [
      theme
        .breakpoints
        .up('sm')
    ]: {
      display: 'block'
    }
  },
  search: {
    marginLeft: '220px !important',

  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  inputRoot: {
    color: 'inherit',
    width: '100%'
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme
      .transitions
      .create('width'),
    width: '100%',
    [
      theme
        .breakpoints
        .up('md')
    ]: {
      width: 200
    }
  },
  sectionDesktop: {
    display: 'none',
    [
      theme
        .breakpoints
        .up('md')
    ]: {
      display: 'flex'
    }
  },
  sectionMobile: {
    display: 'flex',
    [
      theme
        .breakpoints
        .up('md')
    ]: {
      display: 'none'
    }
  },
  hide: {
    display: 'none',
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
    color:'#25265E !important',
  },
  menuButtonWhite: {
    marginLeft: 12,
    marginRight: 36,
    color:'#fff !important',
  },
  avatar: {
    margin: '10px'
  },
  orangeAvatar: {
    margin: '10px',
    fontFamily: 'WhitneyHTF-Medium',
    height: "40px",
    width: "40px",
    fontSize: "12px",
    color: '#fff',
    backgroundColor: blue[500]
  },
  purpleAvatar: {
    margin: 4,
    color: '#fff',
    backgroundColor: deepPurple[500]
  }

});

class PrimarySearchAppBar extends React.Component {
  state = {
    anchorEl: null,
    mobileMoreAnchorEl: null,
    notificationsOpen: false,
    menuOpen: false,
    user: {first_name: '', last_name: '',}, 
    activeuser: {},
    drawerOpen: false
  };

  handleProfileMenuToggle = () => {
    this.setState({
      menuOpen: true
    });
  };
  
  handleProfileMenuClose = () => {
    this.setState({
      menuOpen: false
    });
  };

  handleMenuClose = () => {
    this.setState({anchorEl: null});
    this.handleMobileMenuClose();
  };

  handleMobileMenuOpen = event => {
    this.setState({mobileMoreAnchorEl: event.currentTarget});
  };

  handleMobileMenuClose = () => {
    this.setState({mobileMoreAnchorEl: null});
  };

  handleNoficationsToggle = () => {
    this.setState({
      notificationsOpen: true
    });
  }
  handleNoficationsClose = () => {
    this.setState({
      notificationsOpen: false
    });
  }
  handleDrawerOpen = () => {
    // console.log("Show Drawer")
    if (this.props.showSideBar){
      this.props.showSideBar()
    }
  } 

  
  componentDidMount = () => {
    if (this.props.user !== null){
      this.setState({user: this.props.user})
    }
    console.log(this.props.user, "USER OBJECT")
    if (this.props.user===null){
      window.location.reload()
      // this.props.history.pushState('/')
    } 
    
  }

  componentDidUpdate = (pProps, prevState) => {
    if (this.state.user !== this.props.user && this.props.user !== null){
      this.setState({user: this.props.user})
    } 

  }
  
  componentWillReceiveProps = (nprops) => {


  }
  
  

  render() {
    const {anchorEl} = this.state;
    const {classes} = this.props;
    const isMenuOpen = Boolean(anchorEl);
    const activeuser = JSON.parse(window.localStorage.getItem('activeuser'))
    const currency = window.localStorage.getItem('currency')
    
    const renderNotification = (
      <ClickAwayListener onClickAway={this.handleNoficationsClose}>
        <div className="notification-menu">
          <div className="notification-title">Notifications</div>
          <div className="notification-body">
            <div className="row notification-item">
              <div className="">
                <div className="notification-avatar col-sm-1">
                  <div className="notification-avator-announcement notifcation-avatar"></div>
                </div>
                <div className="notification-text-title col-sm-11">
                  new feature
                </div>
                <div className="notification-text-message col-sm-11">
                  You can now message your clients directly from the user dashboard. To do
                  this, hover on the customer's name ....
                </div>
              </div>

              
            </div>
            <hr className="notification-hr"></hr>
            <div className="row notification-item">
              <div className="">
                <div className="notification-avatar col-sm-1">
                  <div className="notification-avator-error notifcation-avatar"></div>
                </div>
                <div className="notification-text-title col-sm-11">
                  Recommended feature
                </div>
                <div className="notification-text-message col-sm-11">
                  You can quickly assign orders to righters on the recent orders table in the home page. Just click the menu button on the order.
                </div>
              </div>
              
            </div>
            <hr className="notification-hr"></hr>

            <div className="row notification-item">
              <div className="">
                <div className="notification-avatar col-sm-1">
                  <div className="notification-avator-cashflow notifcation-avatar"></div>
                </div>
                <div className="notification-text-title col-sm-11">
                  Milestone
                </div>
                <div className="notification-text-message col-sm-11">
                  Congratulations! You're on a roll. You just reached a monthly order volume
                  of {currency} 250,000.
                </div>
              </div>
            </div>
          </div>

        </div>
      </ClickAwayListener>
    )
    
    const renderDropMenu = (
      <ClickAwayListener onClickAway={this.handleProfileMenuClose}>
        <div className="drop-menu">
          <div className="drop-menu-user">
            <div className="drop-menu-name">{this.state.user.first_name} {this.state.user.last_name}</div>
            <div className="drop-menu-role"> {titleCase(activeuser.user.name)}</div>
          </div>
          <hr className="notification-hr"></hr>
          <div className="drop-menu-list">
          <div><Link className="menu-link" to="/settings/profile">My Profile</Link></div>
          <div><Link className="menu-link" to="/settings">Settings</Link></div>
          <div><Link  className="menu-link" to="/logout">Logout</Link></div>
          </div>
        </div>
      </ClickAwayListener>
    )

    let render_ready = <AppBar position="static" className="topNav zero-left-pad">

    <Toolbar className="zero-left-pad">
    
      {/* <div className="col-sm-2 nav-logo"><img
        src="https://s3.amazonaws.com/dash-user-dashboard/images/white-logo.svg"
        alt="dashboard logo"
        width="60%"/></div> */}
      
      <div className={classes.search + " search"}>
        <div className={classes.searchIcon + " search-icon"}></div>
        <Input
          placeholder="Search …"
          disableUnderline
          classes={{
          root: classes.inputRoot,
          input: classes.inputInput + " search-input"
        }}/>
      </div>
      <div className={classes.sectionMobile}>
      <IconButton
              color='primary'
              aria-label="Open drawer"
              onClick={this.handleDrawerOpen}
              className={this.props.drawerOpen?classes.menuButtonWhite:classes.menuButton}
            >
              <MenuIcon />
            </IconButton>
        </div>
      <div className={classes.grow}/>
      <div className={classes.sectionDesktop}>
        <span className="notification-icon">
        
          <IconButton
            color="default"
            className="nav-button"
            onClick={this.handleNoficationsToggle}>

            <Badge className="topnav-badge" badgeContent={3} color="secondary">
              <span></span>
            </Badge>
            <div className="label">Notifications</div>
          </IconButton>
          <IconButton
            className="nav-button topnav-acc"
            aria-owns={isMenuOpen
            ? 'material-appbar'
            : null}
            aria-haspopup="true"
            onClick={this.handleProfileMenuToggle}
            color="inherit">

            <div className="label dropdown-icon">
              Hello, {this.state.user.first_name}</div>
            <SmartAvatar className="avatar-blue" names={this.state.user.first_name + " " + this.state.user.last_name}/>
          </IconButton>

        </span>
      </div>
      
        
      <div className={classes.sectionMobile}>
        <span className="notification-icon">
          <IconButton
            color="default"
            className="nav-button"
            onClick={this.handleNoficationsToggle}>

            <Badge className="topnav-badge" badgeContent={3} color="secondary">
              <span></span>
            </Badge>
            <div className="label">Notifications</div>
          </IconButton>
          
          <IconButton
            className="nav-button topnav-acc"
            aria-owns={isMenuOpen
            ? 'material-appbar'
            : null}
            aria-haspopup="true"
            onClick={this.handleProfileMenuToggle}
            color="inherit">

            <div className="label dropdown-icon">
              Hello, {this.state.user.first_name}</div>
            <SmartAvatar className="avatar-blue" names={this.state.user.first_name + " " + this.state.user.last_name}/>
          </IconButton>
          

        </span>
      </div>
    </Toolbar>
  </AppBar>
    return (
      
      <div className={classes.root + " nav"}>

        {render_ready}

        
        {this.state.menuOpen
          ? renderDropMenu
          : ""}
        {this.state.notificationsOpen
          ? 
          renderNotification
          : ""}

      </div>
    );
  }
}

PrimarySearchAppBar.propTypes = {
  classes: PropTypes.object.isRequired
};

const user = JSON.parse(localStorage.getItem('user'))


const mapState = (state) => ({
  state: state,
  user: user,
});

export default connect(mapState)(withStyles(styles)(withRouter(PrimarySearchAppBar)));
