import React from 'react';

class SimpleInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.value,
      type: this.props.type,
      shown: true,
      inputValid: null,
      formErrors: {
        email: '',
        password: ''
      }
    };
    this.handleInputChange = this
      .handleInputChange
      .bind(this);
    this.showPasswordToggle = this
      .showPasswordToggle
      .bind(this);
  }
  
  handleInputChange(event) {
    const target = event.target;
    const value = target.value;

    this.setState({value: value});
    try{
      this.props.onChange(target)
    } catch(e) {
      console.log("Couldn't return input value: please provide onchange method in prop that accepts the returned input value")
    }
    

    //the following portion will be deprecated
    try {
      this
        .props
        .updateInputVal(target)
    } catch (e) {
      try {
        this
          .props
          .customFunct(e)
      } catch (e) {}
    }

  }

  showPasswordToggle(e) {
    console.log("state ", this.state)
    this.setState({
      type: this.state.type === "password"
        ? "text"
        : "password",
      shown: !this.state.shown
    });

  }

  render() {
    var passIcon = ""
    if (this.props.type === "password") {
      const iconClass = typeof this.props.iconClass==='undefined'? "password-vision":  this.props.iconClass;
      passIcon = <div
        className={this.state.shown
        ? iconClass
        : iconClass+" shown"}
        id="passwordIcon"
        onClick={this.showPasswordToggle}></div>
    }

    let input = <div className={"col-sm-12 " + this.props.className} style={this.props.style}>
    
    {this.props.label?<label htmlFor={this.props.id} className="cashout-label">{this.props.label}</label>:''}

    <input
      id={this.props.id}
      type={this.state.type}
      className={"simple-input " + this.props.inputClass}
      name={this.props.name}
      placeholder={this.props.placeholder}
      autoComplete="off"
      value={this.state.value}
      maxLength = {this.props.maxLength? this.props.maxLength: ''}
      onChange={this.handleInputChange}/> {passIcon}
  </div>

  let textArea = <div className={"col-sm-12 " + this.props.className} style={this.props.style}>
  <label htmlFor={this.props.id} className="cashout-label">{this.props.label}</label>
  <textarea
    id={this.props.id}
    className={"simple-input " + this.props.inputClass}
    name={this.props.name}
    placeholder={this.props.placeholder}
    autoComplete="off"
    value={this.state.value}
    maxLength = {this.props.maxLength? this.props.maxLength: ''}
    onChange={this.handleInputChange}/> {passIcon}
</div>
    return (
    <div>{this.props.type==='textarea'? textArea: input}</div>
    );
  }

}

export default SimpleInput;
