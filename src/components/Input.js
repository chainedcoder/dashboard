import React from 'react';

class Input extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
      type: this.props.type,
      shown: true,
      inputValid: null,
      formErrors: {
        email: '',
        password: ''
      }
    };

    this.handleInputChange = this
      .handleInputChange
      .bind(this);
    this.showPasswordToggle = this
      .showPasswordToggle
      .bind(this);
    this.handleInputValidate = this
      .handleInputValidate
      .bind(this);

  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;

    this.setState({value: value});
    this.props.onChange(target)
    try {
      this
        .props
        .updateInputVal(target)
    } catch (e) {
      try {
        this.props.customFunct(e)
      } catch (e) {}
    }
  }

  showPasswordToggle(e) {
    this.setState({
      type: this.state.type === "password"
        ? "text"
        : "password",
      shown: !this.state.shown
    });

  }

  componentWillUpdate(props) {};
  componentWillReceiveProps() {}

  handleInputValidate(e) {
    // const type = e.target.type;
    if (this.props.validate) {
      const name = e.target.name;
      const target = e.target
      const value = e.target.value;
      this.setState({
        name: value
      }, () => {
        this.validateField(name, value, target)
      });
    }

  }

  validateField(fieldName, value, target) {
    let fieldValidationErrors = this.props.errors;
    let inputValid = this.state.inputValid;
    // let passwordValid = this.state.passwordValid;
    fieldValidationErrors.Oops = ''

    switch (fieldName) {
      case 'email':
        inputValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        fieldValidationErrors.email = inputValid
          ? ''
          : ' is invalid';
        break;
      case 'password':
        inputValid = value.length >= 6;
        fieldValidationErrors.password = inputValid
          ? ''
          : ' is too short';
        break;
      default:
        break;
    }

    this
      .props
      .validationRes(fieldValidationErrors, inputValid, target)
  }

  render() {
    var passIcon = ""
    if (this.props.type === "password") {
      passIcon = <div
        className={this.state.shown
        ? "password-vision"
        : "password-vision shown"}
        id="passwordIcon"
        onClick={this.showPasswordToggle}></div>
    }

    return (
      <div className="" style={{width:'100%'}}>
        <input
          type={this.state.type}
          className={"dashboard-input " + this.props.classes}
          name={this.props.name}
          placeholder={this.props.placeholder}
          autoComplete="off"
          value={this.state.value}
          onChange={this.handleInputChange} />
        <i className="vline"></i>
        {passIcon}
      </div>
    )
  }
}

export default Input;