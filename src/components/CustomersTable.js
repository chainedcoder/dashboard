import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import FilterIcon from '../images/icons/Filter.svg'
import {withRouter} from 'react-router'
import FilterButton from './FilterButton'
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import API, {ENDPOINTS} from '../Api/Constants'
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import {queryParamsFromLink} from '../actions/urlParams'
import LoadMore from '../images/icons/LoadMoreIcon.svg'
// import { jssPreset } from '@material-ui/core';
import Loader from './Loader';

class CustomersTable extends React.Component {
  constructor() {
    super();
    this.state = {
      optionsOpen: false,
      filterOpen: false,
      selected: {},
      customers: [],
      loading: false
    }

    this.handleShowOptions = this
      .handleShowOptions
      .bind(this)
    this.handleCloseOptions = this
      .handleCloseOptions
      .bind(this)

  }

  handleCustomerClick = (e, id) => {
    console.log(e, id);
    this
      .props
      .history
      .push('/Customers/customer/' + id)
  }


  handleShowOptions(event, id) {
    this.setState({optionsOpen: id, anchorEl: event.currentTarget})
    console.log("open options", this.state, event,)

  }

  handleCloseOptions() {
    this.setState({optionsOpen: false, anchorEl: null})
    console.log("close options", this.state)
  }

  handleClickAwayFilter = () => {
    this.setState({filterOpen: false});
  }

  handleFilterOpen = () => {
    this.setState({
      filterOpen: !this.state.filterOpen
    });
  }

  onChecked = (el, checked) => {
    let newSelected = this.state.selected
    newSelected[el] = checked
    this.setState({selected: newSelected})
    console.log("selected, state: ", newSelected, this.state.selected)
  }

  handleFetchCustomer = () => {
    this.setState({loading: true})
    API
      .get(ENDPOINTS.customers, {params: this.state.transParams, headers: {'X-Spk-Person': 'fkd7ezC5EiIBK-pQNVRlSYiBmM7GakHBctdiI8SAXvtoGkhtYJ31_qKLsynKI3wg9mqNRENxt29lMnhRtNHeSUkiycN4ucCQWJe86_Y3bw491OnJgLtMbccrSVB99nTSWuzrbBAAAAAg',
      }} )
      .then(res => {
        if (res.status === 200) {
          if (res.data.next){
            let nextParams = queryParamsFromLink(res.data.next)
          let transParams = this.state.transParams
          transParams.offset = nextParams.offset
          transParams.limit = nextParams.limit
          this.setState({more: nextParams, transParams: transParams})
          }
          

          const customers = [...this.state.customers, ...res.data.results]
          console.log('****\n',res.data.results, '\n\n\n', this.state.customers)
          
          this.setState({ customers: customers})
        }
        this.setState({loading: false}) 
      })

      .catch((error) => {
        if (error.code === 'ECONNABORTED') {
          console.log('Request canceled', error.message);
          console.log('Retrying.....');
          this.handleFetchCustomer()
        } else {
          this.setState({loading: false})
          console.log("Hmmm...", JSON.stringify(error))
        }

      })
  }

  componentDidMount = () => {
    this.handleFetchCustomer()
  }
  
  loadMore = () => {
    this.handleFetchCustomer()
  }

  render() {

    // let id = 0;
    // // function createData(name, phone, location, amount) {
    // //   id += 1;
    // //   return {id, name, phone, location, amount};
    // // }

    
    let filterId = 0
    const createFilter = (label, value) => {
      filterId += 1;
      return {filterId, label, value}
    }

    const filterItems = [
      createFilter('Successful', "successfulFilter"),
      createFilter('Cancelled', "cancelledFilter"),
      createFilter('Pending', "pendingFilter")
    ]

    let customersData = []
    
    for(let i=0; i<this.state.customers.length; i++){
      let names = this.state.customers[i].first_name + " "+ this.state.customers[i].last_name;
      names = names.toLowerCase();
      const fullName = names.replace(/(^|\s)\S/g, function (t) {
        return t.toUpperCase()
      });
      const phone = this.state.customers[i].phone
      const location = this.state.customers[i].location
      const transactions = this.state.customers[i].transactions

      customersData.push(<TableRow
        key={i}
        className={"" + i === this.state.customers.length
        ? "last-row"
        : ""}>
        <TableCell className="text-blue" component="th" scope="row">
          {/* <SmartAvatar className="avatar-recent" names={fullName}/> */}
          <span>{fullName}</span>
        </TableCell>
        <TableCell className="text-blue" numeric>{phone}</TableCell>
        <TableCell className="text-date-amount" numeric>
          {location}
        </TableCell>
        <TableCell className="text-date-amount" numeric>
        <div style={{display: 'inline-block', padding: '11px 0'}}>{transactions}</div>
          <div  className="pull-right" onClick={this.showOptions} style={{display: 'inline-block', padding: '2px 0'}}>
          <div className="tooltip">
                    <span className="tooltiptext">More Actions</span>
            {/* <img src={`${MoreIcon}`} alt=""></img> */}
            <div className="more-options" onClick={(e) => {this.handleShowOptions(e, i)}}></div>
            </div>
            <Menu className="option-menu" id="render-props-menu" anchorEl={this.state.anchorEl} open={this.state.optionsOpen === i} onClose={this.handleCloseOptions}>
              <MenuItem onClick={(e)=>{this.handleCustomerClick(e,phone)}}>Customer Details</MenuItem>
            </Menu>
          </div>
        </TableCell>
      </TableRow>)

    }
    return (
      <div>
        <Loader open={this.state.loading}/>
        <div className="table card">
        <div className="data-table-title">
          <ClickAwayListener onClickAway={this.handleClickAwayFilter}>
            <div className="pull-left filter-btn">
              <div
                className="card-button pull-left margin-left-20 card-button-customers"
                onClick={this.handleFilterOpen}>

                <img className="filtericon" src={`${FilterIcon}`} alt="filter icon"></img>
                Filter
              </div>
              {this.state.filterOpen
                ? <FilterButton
                    filterItems={filterItems}
                    selected={this.state.selected}
                    onChecked={this.onChecked}/>
                : ""}

            </div>
          </ClickAwayListener>

        </div>
        <Table className={"data-table"}>
          <TableHead>
            <TableRow className="head-row transactions-head-row">
              <TableCell>Name</TableCell>
              <TableCell numeric>Phone Number</TableCell>
              <TableCell numeric>Location</TableCell>
              <TableCell numeric>Orders</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
          { this.state.customers? customersData: '' }
          </TableBody>
        </Table>
      </div>

      {this.state.more?<div className="load-more" onClick={this.loadMore}>Load More
            <span className="load-arrow">
              <img className="down-arrow" src={`${LoadMore}`}  alt="load more icon"></img>
            </span>
          </div>:''}
      </div>
    );
  }

}

export default withRouter(CustomersTable);
