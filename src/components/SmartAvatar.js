import React from 'react';
import Avatar from '@material-ui/core/Avatar';


class SmartAvatar extends React.Component {
  constructor() {
    super();
    this.state = { initials: '', names: '' };
  }

  render() {
    return (
    <Avatar className={"avatar "+this.props.className}>{this.state.initials}</Avatar>
    );
  }


  componentDidUpdate = (pProps, prevState) => {
    if (this.state.names !== this.props.names && this.props.names !== undefined && this.props.names){      
    const names =this.props.names.split(" ")
    const initials = String(names[0][0]) + String(names[names.length-1][0])

    this.setState({ names: this.props.names,initials: initials });
    }
  }
  
  componentDidMount() {
    this.componentDidUpdate()
  }
}

export default SmartAvatar;
