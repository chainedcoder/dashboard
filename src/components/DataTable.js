import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import SmartAvatar from './SmartAvatar';
import STimeAgo from './STimeAgo';
import moment from 'moment';
import API, {ENDPOINTS} from '../Api/Constants'
import Loader from '../components/Loader'

class DateTable extends React.Component {
  constructor() {
    super();
    this.state = {
      transactions: [],
      loading: false
    };
  }

  handleFetchTransactions = () => {
    this.setState({loading: true})
    API
      .get(ENDPOINTS.recentTrans, JSON.stringify())
      .then(res => {

        if (res.status === 200) {
          const trans = res.data.results
          if (trans !== undefined) {
            this.setState({transactions: trans})
          }

          console.log(res, " Got a response")
        }
        if (res.status === 403) {
          this.handleFetchTransactions()
        }
        this.setState({loading: false})

      })
      .catch((error) => {

        if (error.code === 'ECONNABORTED') {
          console.log('Request canceled', error.message);
          console.log('Retrying.....');
          this.handleFetchTransactions()
        } else {
          this.setState({loading: false})

          const res = error.response
          try {
            if (res.status === 403) {
              // this.handleFetchTransactions()
            } else {
              console.log("Hmmm...")
            }

          } catch (e) {
            console.log(e)
          }
        }

      })
  }

  componentDidMount() {
    this.handleFetchTransactions()
  }

  render() {
    // let id = 0; function createData(name, phone, date, amount) {   id += 1;
    // return {id, name, phone, date, amount}; }

    let rowData = []
    // console.log(this.state.transactions, "TRansaction ready")

    for (let i = 0; i < this.state.transactions.length; i++) {
      const fullName = this
        .state
        .transactions[i]
        .person_name
        .toLowerCase();
      const titledNames = fullName.replace(/(^|\s)\S/g, function (t) {
        return t.toUpperCase()
      });
      const phone = this.state.transactions[i].phone
      const date = this.state.transactions[i].created
      var fDate = moment(date).format('lll');
      const amount = this
        .state
        .transactions[i]
        .m_amount
        .toLocaleString(undefined, {maximumFractionDigits: 2})
      const currency = window
        .localStorage
        .getItem('currency')

        rowData.push(
          <TableRow
            key={i}
            className={"" + i === this.state.transactions.length
            ? "last-row"
            : ""}>
            <TableCell className="text-blue" component="th" scope="row">
              <SmartAvatar className="avatar-recent" names={fullName}/>
              <span className="sentence-case">{titledNames}</span>
            </TableCell>
            <TableCell className="text-blue" numeric>{phone}</TableCell>
            <TableCell className="text-date-amount" numeric>
              <div className="tooltip">
                <span className="tooltiptext">{fDate}</span>
                <STimeAgo date={date}></STimeAgo>
              </div>
            </TableCell>
            <TableCell className="text-date-amount" numeric>{currency} {amount}</TableCell>
          </TableRow>
        )

    }

    return (
      <div className="">
        <Loader open={this.state.loading}/>

        <div className="data-table-title">
          <div className="table-title">
            Recent Orders
          </div>
          <a href="/transactions/">
            <div className="card-button sub-title pull-right">View All</div>
          </a>
        </div>
        <Table className={"data-table table "}>
          <TableHead>
            <TableRow className="head-row">
              <TableCell>Title</TableCell>
              <TableCell numeric>Client Name</TableCell>
              <TableCell numeric>Deadline</TableCell>
              <TableCell numeric>Amount</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.state.transactions
              ? rowData
              : ''}
          </TableBody>
        </Table>
      </div>
    );
  }

}

export default DateTable;
