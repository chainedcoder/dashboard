import React from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox';

class FilterButton extends React.Component {
  constructor() {
    super();
    this.state = {
      optionsOpen: false,
      selected: this.props
    };
    this.handleCheckFilter = this
      .handleCheckFilter
      .bind(this);
  }

  handleCheckFilter(e, name) {
    const checked = e.target.checked
    let selected = {}
    selected[name] = checked
    this.setState({selected: selected});
    this.props.onChecked(name, checked)
  }

  render() {
    const filterItems = this.props.filterItems
    return (

      <div className={"card paper filters "}>
      <div className="filter-head">Filters</div>
        {filterItems.map(filterItem => {
          const isSelected = this.state.selected[filterItem.value]===undefined?false:this.state.selected[filterItem.value]
          return (
            <FormControlLabel
                key={filterItem.filterId}
                className="filter-item"
              control={< Checkbox checked = {
              isSelected
            }

            onChange = {
              (e) => {
                this.handleCheckFilter(e, filterItem.value)
              }
            }

            name={filterItem.value}
            value = {
              filterItem.value
            }
            color = "primary" />}
              label={filterItem.label}/>
          );
        })}

        
      </div>
    );
  }

  componentWillMount() {
    const  selected = this.props.selected
    this.setState({selected:selected})
  }

  componentWillReceiveProps (){
  }
  componentWillUpdate(){
      
    const  selected = this.props.selected
    if(selected !==this.state.selected){
        this.setState({selected:selected})
    }
    
  }
}

export default FilterButton;
