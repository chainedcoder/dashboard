import React from 'react'
import IconButton from '@material-ui/core/IconButton';
// import Badge from '@material-ui/core/Badge';
import PropTypes from 'prop-types';

class MenuItem extends React.Component {
  constructor(props)
  {
    super(props)
    this.state = {
      active: this.props.state
    }
  }

  render() {

    var isActive = this
      .context
      .router
      .route
      .location
      .pathname
      .split('/')[1] === this
      .props
      .to
      .split('/')[1];
    var classActive = isActive
      ? " active-nav "
      : "";

    let el = <a href={this.props.to}>
      <IconButton
        color="default"
        className={"sidenav-button " + classActive + this.props.className}
        disableRipple={true}>
        <img src={`${this.props.icon}`} alt=""></img>
        <div className="label">{this.props.children}</div>
      </IconButton>
    </a>

    const onchange = this.props.onchange
    if (onchange) {
      el = <a >
        <IconButton
          color="default"
          className={"sidenav-button " + classActive + this.props.className}
          disableRipple={true}>
          <img src={`${this.props.icon}`} alt=""></img>
          <div className="label">{this.props.children}</div>
        </IconButton>
      </a>
    }
    return (
      <div className=" col-sm-12" onClick={this.props.onchange}>
        {el}
      </div>
    )
  }
}

MenuItem.contextTypes = {
  router: PropTypes.object
};

export default MenuItem;