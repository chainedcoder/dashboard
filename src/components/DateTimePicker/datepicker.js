import * as React from 'react'
import {withStyles} from '@material-ui/core/styles'
import Popover from '@material-ui/core/Popover'
import moment from 'moment';
import Calendar from './calendar' //import Calendar, {CalendarProps} from './calendar'

const styles = () => ({
  formControl: {
    cursor: 'pointer'
  }
})

class DateFormatInput extends React.Component {
  action = {}
  calendar
  constructor(props) {
    super(props)
    this.state = {
      calendarShow: true
    }
  }
  componentWillReceiveProps() {

  }

  closeCalendar = () => {
    this.props.onClose();
  }

  render() {
    const {
      value,
      onChange,
      transformOrigin,
      dateDisabled,
      min,
      max,
      CalendarProps,
      className,
      // classes
    } = this.props
    // const {focus, calendarShow} = this.state

    const formatedValue = moment(value).toDate()
    const calendarProps = {
      formatedValue,
      onChange,
      dateDisabled,
      min,
      max,
      closeCalendar: this.closeCalendar,
      ...CalendarProps
    }

    return (
      <Popover
            className={"calendar-root " + className}
            key='date-popover'
            
            open={true}
            onClose={this.closeCalendar}
            transformOrigin={transformOrigin}
            anchorEl={this.props.anchorOrigin}>
            <Calendar
              {...calendarProps }/>
          </Popover>
    )
  }
}

export default withStyles(styles)(DateFormatInput)