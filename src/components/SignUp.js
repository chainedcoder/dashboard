import React, {Component} from 'react'
import Input from './Input'
import {Redirect} from 'react-router-dom'
import {sessionService} from 'redux-react-session';
import API, {ENDPOINTS} from '../Api/Constants'
import Button from './Button'
import Grid from '@material-ui/core/Grid';
import ValidationErrors from './ValidationErrors'

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formErrors: {
        email: '',
        password: '',
        Oops: ''
      },
      success: false,
      formValid: false,
      animate: false,
      waitForResponse: false,
      emailValue: '',
      emailValid: true,
      firstnameValue: '',
      firstnameValid: true,
      lastnameValue: '',
      lastnameValid: true,
      SigninRedirect: false

    };
    this.handleBtnAnimate = this
      .handleBtnAnimate
      .bind(this);

    this.setSigninRedirect = this
      .setSigninRedirect
      .bind(this)
  }

  setSigninRedirect = () => {
    console.log("Redirect")
    this.setState({SigninRedirect: true})
  }
  goToSignin = () => {
    if (this.state.SigninRedirect) {
      return <Redirect to='/log-in'/>
    }
  }

  signup = (user) => {
    API
      .post(ENDPOINTS.signup, JSON.stringify(user))
      .then(res => {
        console.log(res);

        if (res.status === 200) {
          // console.log("Success!")
          const {token} = res.data;
          console.log(token)
          sessionService
            .saveSession({token})
            .then(() => {
              sessionService
                .saveUser(res.data)
                .then(() => {
                  console.log(token)
                  this.setState({success: true})
                })
            })
        }

      })
      .catch((error) => {
        const res = error.response
        try {
          const err = res.data.error !== undefined
            ? res.data.error
            : 'Oops, Something went wrong. Try again.'

          if (res.status === 400) {
            console.log(err)
            this.setState({animate: false, active: true, waitForResponse: false, errors: err})
          } else {
            console.log(err)
            this.setState({animate: false, active: true, waitForResponse: false, errors: err})
          }
        } catch (e) {
          this.setState({animate: false, active: true, waitForResponse: false, errors: "Something isn't right. Check your connect and try again."})

        }
        console.log("Error: ", error, "Res: ", error.response);

      })

  };

  handleBtnAnimate(e) {
    e.preventDefault();
    this.validateName(this.state.lastnameValue, this.state.firstnameValue)
    this.validateEmail(this.state.emailValue)
    const formValid = !this.state.emailError && this.state.emailValue && !this.state.firstnameError && this.state.firstnameValue && !this.state.lastnameError && this.state.lastnameValue
    console.log(this.state, "Form valid: ", formValid)
    this.setState({formValid: formValid})
    if (!this.state.waitForResponse && formValid) {
      this.setState({animate: true, active: false, waitForResponse: true});
      const user = {
        first_name: this.state.firstnameValue,
        last_name: this.state.lastnameValue,
        email: this.state.emailValue
      };

      console.log('Making login request: ', user, JSON.stringify(user))

      // const {login} = this.props.actions;
      this.signup(user)
      // this.setState(stateRes)
      console.log(this.state, "AFter login")
    }

  }

  onInputName = (target) => {
    const name = target.name + "Value"

    this.setState({[name]: target.value})

    this.validateName(target.value)
    console.log(name, "::", target.value, this.state)
  }

  validateName = (fname, lname) => {
    if ((fname === undefined || fname === '') && (lname === undefined || lname === '')) {
      this.setState({NameError: "No empty field allowed"})
    }
  }

  onInputEmail = (target) => {
    const name = target.name + "Value"

    this.setState({[name]: target.value})

    this.validateEmail(target.value)
    console.log(name, "::", target.value, this.state)
  }

  validateEmail = (value) => {
    if (value === undefined || value === '') {
      this.setState({emailError: "Please Enter Your Email"})
    } else if (value) {
      const inputValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
      var emailError = inputValid
        ? ''
        : 'Email is invalid';
      this.setState({emailError: emailError})
    }
  }

  render() {
    const signup = <Grid container spacing={0}>
      <Grid className="" item xs={1} sm={2} lg={2} md={2}></Grid>
      <Grid className="" item xs={10} sm={8} lg={8} md={8}>
        <Grid container spacing={0}>
          <Grid className="" item xs={12} sm={12} md={12} lg={12}>
            <form method="post" className="landing-form ">
              <Grid container spacing={0}>
                <Grid className="" item xs={12} sm={12} lg={12} md={12}>

                  <div className="spekra-logo"><img
                    src="https://s3.amazonaws.com/dash-user-dashboard/images/white-logo.svg"
                    alt="dashboard logo"/></div>
                </Grid>
                <Grid className="" item xs={12} sm={12} lg={12} md={12}>

                  <div className="title">SIGN UP</div>
                </Grid>

                <br></br>
                <br></br>
                <Input
                  errors={this.state.formErrors}
                  type="text"
                  classes="icon-username"
                  label="firstname"
                  name="firstname"
                  placeholder="First Name"
                  onChange={this.onInputName}
                  validate={true}/>
                <Input
                  type="text"
                  classes="icon-username"
                  label="lastname"
                  name="lastname"
                  placeholder="Last Name"
                  onChange={this.onInputName}
                  validate={true}/>
                <Input
                  errors={this.state.formErrors}
                  type="email"
                  classes="icon-email"
                  label="Username"
                  name="email"
                  placeholder="Email Address"
                  onChange={this.onInputEmail}
                  validate={true}/>
                <ValidationErrors
                  errors={[this.state.firstnameError, this.state.lastnameError, this.state.emailError, this.state.errors]}/>
                <button
                  className="signup-purple-btn"
                  type="button"
                  id=""
                  onClick={this.handleBtnAnimate}>
                  {this.state.animate
                    ? <span
                        className={this.state.animate
                        ? "spinner"
                        : ""}></span>
                    : "SIGN UP"}</button>
                {/* <Button type="submit" animate={true} className="signup-purple-btn" text="SIGN UP" name="" id=""/> */}

                <br></br>

                <hr className="col-sm-12  dashboard-or"></hr>
                {this.goToSignin()}
                <button onClick={this.setSigninRedirect} className="signup-btn col-sm-12">LOG IN</button>
              </Grid>
            </form>

          </Grid>
        </Grid>
      </Grid>
    </Grid>

    const success = <Grid container spacing={0}>
      <Grid className="" item xs={1} sm={2} lg={2} md={2}></Grid>
        <Grid className="" item xs={10} sm={8} lg={8} md={8}>
        <form method="post" className="landing-form">
        <Grid container spacing={0}>
            <div className="spekra-logo"><img
              src="https://s3.amazonaws.com/dash-user-dashboard/images/white-logo.svg"
              alt="dashboard logo"/></div>
            <div className="title-text">Account successfully created! check your email now to verify it.</div>
            <br></br>
            <div className="title-text">Didn't receive the email? click below to resend.</div>
            <br></br>

            <Button
              animate={true}
              className="signup-purple-btn col-sm-12 "
              text="RESEND EMAIL"
              name=""
              id=""/>

          </Grid>
        </form>

      </Grid>
    </Grid>
    return (
      <div>{this.state.success
          ? success
          : signup}</div>

    );
  }
}

export default SignUp;