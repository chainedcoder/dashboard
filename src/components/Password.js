
import React from 'react';

class Password extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          value: "",
          type: this.props.type,
          shown: true
        };
    
        this.handleInputChange = this.handleInputChange.bind(this);
        this.showPasswordToggle = this.showPasswordToggle.bind(this);
    }
    
    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
    
        this.setState({
          value: value
        });
    }

    showPasswordToggle(e){
        console.log("state ", this.state)
        this.setState({
          type: this.state.type === "password" ? "text" : "password",
          shown: !this.state.shown

          }); 

    }

    componentWillUpdate (props) {

    };

    render(){


        return (
            <div>
                <input 
                    type={this.state.type} 
                    className={"dashboard-input " + this.props.classes} 
                    name={this.props.name} 
                    placeholder={this.props.placeholder}
                    autoComplete ="off"
                    value={this.state.value}
                    onChange={this.handleInputChange}
                /> 
                <i className="vline"></i>
            </div>
        )
    }
}

export default Password;