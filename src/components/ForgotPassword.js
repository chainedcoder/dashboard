import React, {Component} from 'react'
import Input from './Input'
import Button from './Button'

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      emailValid: true
    };
    this.verfyEmail= this
      .verifyEmail
      .bind(this);
    
  }

  verifyEmail(e){
    const valid = e.target.value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
    this.setState({emailValid:valid})
    return valid
  }

  render() {

    return (
      <div className="row">
        <div className="col-sm-offset-2">
          <form method="post" className="landing-form col-sm-10 ">
            <div className="row">
              <div className="spekra-logo"><img
                src="https://s3.amazonaws.com/dash-user-dashboard/images/white-logo.svg"
                alt="dashboard logo"/></div>
              <div className="title-text">Enter Your Email To Reset Your Password ...</div>
              <br></br>
              <br></br>
              <Input
                type="email"
                classes="icon-email"
                label="Email"
                name="email"
                placeholder="Your Email Address"
                customFunct = {this.verifyEmail}
                />
                <div className="formErrors">{this.state.emailValid?"":<p>Invalid Email</p>}</div>

              <Button
                animate={true}
                className="signup-purple-btn col-sm-12 "
                text="CONTINUE"
                name=""
                id=""/>
                
            </div>
          </form>

        </div>
      </div>
    );
  }
}

export default ForgotPassword;