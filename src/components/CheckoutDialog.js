import React from 'react';
import Dialog from '@material-ui/core/Dialog';

class CheckoutDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = false;
  }

  render() {
    // this.setState({ open: this.props.open });
    return (
        <Dialog
          open={typeof this.state.open === "undefined"?false:this.state.open}
          aria-labelledby="form-dialog-title"
          className="dialog">
          <div className="dialog-content">
            <div className="cashout-dialog-title">
              {this.props.title==="undefined"?this.props.title:"Cash Out"}
            </div>
            <div className="dialog-body">
              {this.props.children}

            </div>
          </div>

        </Dialog>
    );
  }
  updateOpenState(){
    if (this.state.open !== this.props.open && this.state.open!=="undefined"){
      this.setState({ open: this.props.open });
    }
  }
  componentDidUpdate(){ 
    this.updateOpenState()
  }

componentWillMount(){
  this.updateOpenState()
}
  componentWillReceiveProps(){
    this.updateOpenState()
  }
}

export default CheckoutDialog;
