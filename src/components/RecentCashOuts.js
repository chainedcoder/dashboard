import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import FilterIcon from '../images/icons/Filter.svg'
import {withRouter} from 'react-router'
import CashOutItem from './CashOutItem'
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import DateRangePicker from './DateRangePicker';
import FilterButton from './FilterButton'


// import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
class RecentCashOuts extends React.Component {
  constructor() {
    super();
    this.state = {
      optionsOpen: false,
      filterOpen: false,
      successFilter: "",
      pendingFilter: "",
      cancelledFilter: "",
      selected: {}
    };

    this.handleTransRowClick = this
      .handleTransRowClick
      .bind(this);
    this.handleCheckFilter = this
      .handleCheckFilter
      .bind(this);
  }

  handleTransRowClick = (id) => {
    console.log(this.props);
    this
      .props
      .history
      .push('/cashout/details/' + id)
  }

  handleRefunds = event => {
    this.setState({anchorEl: event.currentTarget});
  }

  handleCloseOptions = () => {
    this.setState({optionsOpen: false, anchorEl: null});
  };

  handleShowOptions(event, id) {
    this.setState({optionsOpen: id, anchorEl: event.currentTarget})
  }

  handleClickAwayFilter = () => {
    this.setState({filterOpen: false});
  }

  handleFilterOpen = () => {
    this.setState({
      filterOpen: !this.state.filterOpen
    });
  }

  handleCheckFilter(e, name) {
    this.setState({[name]: e.target.checked, filterOpen: true});
    console.log(this.state, e)
  }

  handleClickAwayFilter = () => {
    this.setState({filterOpen: false});
  }

  handleFilterOpen = () => {
    this.setState({
      filterOpen: !this.state.filterOpen
    });
  }

  onChecked = (el, checked) => {
    let newSelected =  this.state.selected
    newSelected[el] = checked
    this.setState({selected:newSelected})
    console.log("selected, state: ", newSelected,  this.state.selected)
  }

  render() {

    let id = 0;
    function createData(name, phone, date, amount) {
      id += 1;
      return {id, name, phone, date, amount};
    }

    var rows = [
      createData('UT-NPO-101DAZ1', "Bank", "15/02/2019 12:00:12", "5,244"),
      createData('UT-MPO-21KJHS2', "Paypal", "2/6/2019 12:00:12", "7,377"),
      createData('UT-R2U-XM7635M', "Bank", "3/06/2019 08:17:12", "10,267"),
      createData('UT-R2B-09MSHAS', "Bank", "28/03/2019 12:00:12", "6,567"),
      createData('UT-R2B-OLASD11', "Bank", "28/09/2018 12:00:12", "4,657")
    ];
    if (typeof this.props.maxRows !== 'undefined') {
      rows = rows.slice(0, this.props.maxRows)
    }

    let filterId = 0
    const createFilter = (label, value) => {
      filterId += 1;
      return {filterId, label, value}

    }

    const filterItems = [
      createFilter('Successful', "successfulFilter"),
      createFilter('Cancelled', "cancelledFilter"),
      createFilter('Pending', "pendingFilter")
    ]

    // const fake = <div className="fake"/>;

    return (
      <div className="table card">
        <div className="data-table-title">
        <ClickAwayListener onClickAway={this.handleClickAwayFilter}>
            <div className="pull-left filter-btn">
              <div
                className="card-button pull-left margin-left-20 card-button-customers"
                onClick={this.handleFilterOpen}>

                <img className="filtericon" src={`${FilterIcon}`} alt="filter icon"></img>
                Filter
              </div>
              {this.state.filterOpen?<FilterButton filterItems={filterItems} selected={this.state.selected} onChecked={this.onChecked}/>:""}
              
              </div>
          </ClickAwayListener>
          <div className="pull-right">
            <DateRangePicker/>
          </div>

        </div>
        <Table className={"data-table"}>
          <TableHead>
            <TableRow className="head-row transactions-head-row">
              <TableCell>Transaction ID</TableCell>
              <TableCell >Sent To</TableCell>
              <TableCell >Date</TableCell>
              <TableCell  >Amount</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map(row => {
              return (
                <CashOutItem key={row.id} row={row} rows={rows} parent={this}>
                  <TableCell className="table-cell text-date-amount" ><div className="" style={{display: 'inline-block', padding: '11px 0'}}>{row.amount}</div>
                    <div className="pull-right"  style={{display: 'inline-block', padding: '2px 0'}}>
                      <div className="tooltip">
                        <span className="tooltiptext">More Actions</span>
                        <div className="more-options" onClick={(e) => {this.handleShowOptions(e, row.id)}} ></div>
                      </div>
                      <Menu
                        className="option-menu"
                        id="render-props-menu"
                        anchorEl={this.state.anchorEl}
                        open={this.state.optionsOpen === row.id}
                        onClose={this.handleCloseOptions}>
                        <MenuItem
                          onClick={(event) => {
                          this.handleTransRowClick(row.id)
                        }}>Cashout Details</MenuItem>
                         <MenuItem
                          onClick={(event) => {
                          this.handleTransRowClick(row.id)
                        }}>Re-Do This Cashout</MenuItem>
                      </Menu>
                    </div>
                  </TableCell>
                </CashOutItem>
              );
            })}
          </TableBody>
        </Table>
      </div>
    );
  }

  componentDidMount() {}
}

export default withRouter(RecentCashOuts);
