import React from 'react';
import TopNav from './TopNav'
import MenuItem from './MenuItem'
import HomeIcon from '../images/icons/Home.svg'
import TransIcon from '../images/icons/Transactions.svg'
import CustomersIcon from '../images/icons/Customers.svg'
import CashoutIcon from '../images/icons/Cashout.svg'
import ContactIcon from '../images/icons/Contact.svg' 
import ReportsIcon from '../images/icons/Reports.svg' 
import SettingsIcon from '../images/icons/Settings.svg'
import WriterIcon from '../images/icons/Writer2.svg'
import ChatIcon from '../images/icons/Chat.svg'
import DevelopersIcon from '../images/icons/Developers.svg'
import API, {ENDPOINTS} from '../Api/Constants'
import moment from 'moment'
import {withRouter} from 'react-router'
import ContactUs from '../pages/ContactUs'
import store from '../redux/index.js'
import {showNotificationBar} from '../redux/actions.js'
import Badge from '@material-ui/core/Badge';


class BaseDashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mSidebarOpen: false,
      lastRefresh: moment(),
      tokenReset: false,
      contactUsOpen: false,
      lastRequest: moment().subtract(61, "seconds")

    }
    this.resetTimer =this.resetTimerFn.bind(this)

    // var t; var resetTimer;
  }
  async resetTimerFn() {

    var lastRefresh = await window
      .localStorage
      .getItem('time')
    const lastRefDiff = await moment().diff(lastRefresh, 'seconds')
    const lastReqDiff = await moment().diff(this.state.lastRequest, 'seconds')

    console.log(lastRefDiff, lastReqDiff, "Diff")
    if (lastRefDiff > (60 * 1) && lastReqDiff > (60 * 1)) {
      this.setState({lastRequest: moment()})
      this.refreshToken().then(() => {
        console.log(this.state)
        if (this.state.tokenReset) {
          // this.setState({tokenReset: false})
          console.log(this.t)
          clearInterval(this.t);
          this.t = setInterval(() => {
            store.dispatch(showNotificationBar({message: 'Sorry, Your Session Timed Out. Login Again!', timeout: 1}))

            this
              .props
              .history
              .push('/logout')
          }, 1000 * 60 * 10); //4 minutes
        }
      });
    }

  }

  refreshToken = async() => {
    await API
      .post(ENDPOINTS.refreshToken)
      .then(res => {
        if (res.token) {
          window
            .localStorage
            .setItem('token', res.token)
          window
            .localStorage
            .setItem('time', moment())
          this.setState({tokenReset: true})
        }
      })
      .catch((error) => {
        if (error.code === 'ECONNABORTED') {
          console.log('Request canceled', error.message);
          this.refreshToken()
        } else {
          const res = error.response
          try {
            if (res.status === 403) {
              this
                .props
                .history
                .push('/logout')
            } else {
              console.log("Hmmm...")
            }

          } catch (e) {
            console.log(e)
          }
        }

      });
  }

  contactUs = () => {
    console.log('contact us')
    this.setState({contactUsOpen: true})
  }
  componentDidMount = () => {
    this.t = setTimeout(() => {
      store.dispatch(showNotificationBar({message: 'You Session Has Timed Out, Please Log In Again!', timeout: 1}))

      this
        .props
        .history
        .push('/logout')
    }, 1000 * 60 * 10);

    window.addEventListener('scroll', this.resetTimer);
    window.addEventListener('click', this.resetTimer); 
    window.addEventListener('keypress', this.resetTimer); 
    window.addEventListener('touchstart', this.resetTimer); 
    window.addEventListener('mousedown', this.resetTimer); 


  }
  componentWillUnmount = () => {
    window.removeEventListener('click', this.resetTimer);
    window.removeEventListener('scroll', this.resetTimer);
    window.removeEventListener('keypress', this.resetTimer);
    window.removeEventListener('load', this.resetTimer);
    window.removeEventListener('mousedown', this.resetTimer);
    window.removeEventListener('touchstart', this.resetTimer);

  }

  closeContactUs = () => {
    this.setState({contactUsOpen: false})
  }

  showSideBar = () => {
    this.setState({mSidebarOpen: !this.state.mSidebarOpen})
    console.log("Now show sidebar")
  }
  render() {
    return (
      <div>
        <TopNav showSideBar={this.showSideBar} drawerOpen={this.state.mSidebarOpen}/>

        <div className="dashboard-content">
          <div className="sidebar " style={this.state.mSidebarOpen?{marginLeft:0}:{}}>
            <div className="menu-title">Menu</div>
            <div className='row'>
            <MenuItem to="/home" icon={HomeIcon} className="">Home</MenuItem>
            <MenuItem to="/orders" icon={TransIcon}>Orders 
            <Badge className="sidenav-badge " badgeContent={3} color="secondary">
                <i></i>
              </Badge>
            </MenuItem>
            <MenuItem to="/customers" icon={CustomersIcon}>Clients</MenuItem>

            <MenuItem to="/writers" icon={WriterIcon}>My Writers</MenuItem>
            <MenuItem to="/cashout" icon={CashoutIcon}>Transactions</MenuItem>
            <MenuItem to="/chat" icon={ChatIcon}>Chat</MenuItem>
            <MenuItem to="/settings" icon={SettingsIcon}>Settings</MenuItem>
            <MenuItem to="/reports" icon={ReportsIcon}>Reports
            </MenuItem>
            <div className="col-sm-12">
              <hr></hr>
            </div>
            </div>
            <div className="menu-title">Support</div>
            <div className='row'>
            <MenuItem to="/developers" icon={DevelopersIcon}>Developer</MenuItem>
            {/* <MenuItem to="/help" icon={HelpIcon}>Need Help?</MenuItem> */}
            {/* <MenuItem to="/contact-us" icon={ContactIcon} onchange={this.contactUs}>Contact Us</MenuItem> */}
            </div>

          </div>
          <div className="content-body">
            <div className="top-nav-allowance"></div>
            <div className="content-area">
              {this.props.children}

              <ContactUs open={this.state.contactUsOpen} onclose={this.closeContactUs}/>

            </div>

          </div>

        </div>

      </div>
    )
  }
}

export default withRouter(BaseDashboard);