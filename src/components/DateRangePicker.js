import React from 'react';
import moment from 'moment';
// import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import DateFormatInput from './DateTimePicker/datepicker'
import CalendarIcon from '../images/icons/Calender.svg'
import RightArrow from '../images/icons/CalenderTo.svg'

class DateRangePicker extends React.Component {
  constructor() {
    super();
    this.state = {
      datePickerToOpen: false,
      datePickerFromOpen: false,
      dateTo: moment().format('L'),
      dateFrom: moment().subtract(1, 'month').format('L'),
      min: moment().subtract(1, "years").toDate(),
      max: moment().toDate(),
      
    };

    this.calendarAnchor = React.createRef()
    this.handleDatePickerTo = this
      .handleDatePickerTo
      .bind(this);
    this.handleDatePickerFrom = this
      .handleDatePickerFrom
      .bind(this);
      this.handleCloseCalendar = this.handleCloseCalendar.bind(this)
  }

  onChangeDateTo = (date) => {
    this.setState({
      dateTo: moment(date).format('L'),
      datePickerToOpen: false,
      datePickerFromOpen: false
    })
    console.log("Date from and to ", moment(this.state.dateFrom))

    try {
      this.props.onChange(moment(this.state.dateFrom), moment(this.state.dateTo))

    } catch(e){
      console.log("Provide an onchange to return the date from clendar date picker.")
    }
  }

  onChangeDateFrom = (date) => {
    this.setState({
      dateFrom: moment(date).format('L'),
      datePickerToOpen: true,
      datePickerFromOpen: false
    })
    
  }

  handleDatePickerTo(event) {
    console.log(this.calendarAnchor, " Calendar anchor")
    this.setState({
      anchorDP: this.calendarAnchor.current,
      datePickerToOpen: !this.state.datePickerToOpen

    })
  }

  handleDatePickerFrom(event) {
    this.setState({
      anchorDP: this.calendarAnchor.current,
      datePickerFromOpen: !this.state.datePickerFromOpen

    })
  }

  handleCloseCalendar(){
      this.setState({datePickerFromOpen:false, datePickerToOpen:false})
  }

  render() {
    const datepickerFrom = <DateFormatInput
      show={this.state.datePickerFromOpen}
      anchorOrigin={this.state.anchorDP}
      name='date-input'
      className="calendar-root-from"
      value={this.state.dateFrom}
      min = {this.state.min}
      max = {this.state.max}
      onClose = {this.handleCloseCalendar}
      onChange={(date) => {
      this.onChangeDateFrom(date)
    }}></DateFormatInput>
    const datepickerTo = <DateFormatInput
      show={this.state.datePickerToOpen}
      anchorOrigin={this.state.anchorDP}
      className="calendar-root-to"
      name='date-input'
      min = {this.state.min}
      max = {this.state.max}
      onClose = {this.handleCloseCalendar}
      value={this.state.dateTo}
      onChange={(date) => {
      this.onChangeDateTo(date)
    }}></DateFormatInput>
    return (
      <div className="pull-right">
        <div className="card-button pull-right date-box card-button-transactions">
          <div
            ref ={this.calendarAnchor}
            onClick={this.handleDatePickerFrom}
            style={{
            display: "inline-flex"
          }}>
            <div style={{
              display: "inline-flex"
            }}>
              <img className="calendar-icon " src={`${CalendarIcon}`} alt="calendar icon"></img>
            </div>
            <div className=" date">{this.state.dateFrom}</div>
          </div>

          <img className="right-arrow" src={`${RightArrow}`} alt="right arrow icon"></img>

          <div
            onClick={this.handleDatePickerTo}
            style={{
            display: "inline-flex"
          }}>
            <div style={{
              display: "inline-flex"
            }}>
              <img className="calendar-icon " src={`${CalendarIcon}`} alt="calendar icon"></img>
            </div>
            <div className=" date">{this.state.dateTo}</div>
          </div>

        </div>
        {this.state.datePickerToOpen
          ? datepickerTo
          : ""}

        {this.state.datePickerFromOpen
          ? datepickerFrom
          : ""}
      </div>
    );
  }

  componentDidMount() {
    this.setState({someKey: 'otherValue'});
  }
}

export default DateRangePicker;
