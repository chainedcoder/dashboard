import React, {
    Component
  } from 'react'
  
class Carousel extends Component {
    constructor(props) {
        super(props);
        this.state = {
          active: 1,
          items: [
              {bgImage: "https://s3.amazonaws.com/dash-user-dashboard/images/Image2x.png", text: "Simple: Having lots of features is nice, BUt an easier quicker way of doing things is better."},
              {bgImage: "https://s3.amazonaws.com/dash-user-dashboard/images/Image1x.png", text: "Resolve Disputes: Issue Refunds Instantly To Your Clients."},
              {bgImage: "https://s3.amazonaws.com/dash-user-dashboard/images/Image4x.png", text: "Faster Writers and CLient Management. Don't be Bog Down With Lengthy Processes."}
          ]
        };
    
        this.activateIndicator = this.activateIndicator.bind(this);
    }
    activateIndicator (e){
        const active_id = parseInt(e.target.id)
        this.setState({
          active: active_id,
        });
    }

    render() {
        const carouselItems = this.state.items;

        const carouselIndicator = () => {
            var spans = [];
            
            for (var i = 0; i < carouselItems.length; i++){
                var addCl = ""
                if(i===this.state.active){
                    addCl = "active"
                }
                spans.push(<span id={i} className="indicator-area" onClick={this.activateIndicator}><span id={i} className={"indicator " + addCl} onClick={this.activateIndicator}></span></span>)
            }
            return spans;
        }

        const carouselText = () => {
            var texts = [];
            for (var i = 0; i < carouselItems.length; i++){
                var addCl = ""
                if(i!==this.state.active){
                    addCl = "hidden"
                }
                texts.push(<div className={"carousel-highlite " + addCl}> {carouselItems[i].text} </div>)
            } 
            return texts;
        }

        return (
            <div className="carousel-body">
                {carouselIndicator() }
                {carouselText()}

            </div>
    );
    }
  }
  
  export default Carousel;