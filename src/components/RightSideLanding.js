
import React from 'react';
import image1 from '../images/Image1x.png';
// import Carousel from './Carousel';

class RightSideLanding extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          active: 1,
          items: [
            {bgImage: "https://s3.amazonaws.com/dash-user-dashboard/images/Image2x.png", text: "Simple: Having lots of features is nice, BUt an easier quicker way of doing things is better."},
            {bgImage: "https://s3.amazonaws.com/dash-user-dashboard/images/Image1x.png", text: "Resolve Disputes: Issue Refunds Instantly To Your Clients."},
            {bgImage: "https://s3.amazonaws.com/dash-user-dashboard/images/Image4x.png", text: "Faster Writers and CLient Management. Don't be Bog Down With Lengthy Processes."}
       ],
          background: image1
        };
    
        this.activateIndicator = this.activateIndicator.bind(this);
        this.randomizeCarousel = this.randomizeCarousel.bind(this)
    }
    randomizeCarousel(){
        const active_id = Math.floor((Math.random() * this.state.items.length));
        console.log(active_id, " of ", this.state)
        const bg = this.state.items[active_id].bgImage
        this.setState({
          active: active_id,
          background: bg
        });

    }
    activateIndicator (e){
        const active_id = parseInt(e.target.id, 10)
        const bg = this.state.items[active_id].bgImage
        this.setState({
          active: active_id,
          background: bg
        });
    }

    componentWillMount(){
        this.randomizeCarousel()
    }

    render(){
        const carouselItems = this.state.items;

        // document.getElementById("carousel").style.background = carouselItems[this.state.active].bgImage;

        const carouselIndicator = () => {
            var spans = [];
            
            for (var i = 0; i < carouselItems.length; i++){
                var addCl = ""
                if(i===this.state.active){
                    addCl = "active";
                }
                spans.push(<span key={i+'_'} id={i} className="indicator-area" onClick={this.activateIndicator}><span id={i+'_'} className={"indicator " + addCl} onClick={this.activateIndicator}></span></span>)
            }
            return spans;
        }

        const carouselText = () => {
            var texts = [];
            for (var i = 0; i < carouselItems.length; i++){
                var addCl = ""
                if(i!==this.state.active){
                    addCl = "hidden"
                }
                texts.push(<div key={i} className={"carousel-highlite " + addCl}> {carouselItems[i].text} </div>)
            } 
            return texts;
        }

        return (
            
            <div ref={this.carousel} id="carousel" style={{ backgroundImage: `url(${this.state.background})`}}  className={"right-landing " + this.props.classes}>
                <div className="carousel-body">
                    {carouselIndicator() }
                    {carouselText()}

                </div>
            </div>
        )
    }
}

export default RightSideLanding;
