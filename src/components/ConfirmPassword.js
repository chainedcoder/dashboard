import React, {Component} from 'react'
import Input from './Input'
import Button from './Button'

class SignUpConfirm extends Component {
  render() {
    return (
      <div className="row">
      <div className="col-sm-offset-2">
        <form method="post" className="landing-form col-sm-10 ">
        <div className="row">
          <div className="spekra-logo"><img
            src="https://s3.amazonaws.com/dash-user-dashboard/images/white-logo.svg"
            alt="dashboard logo"/></div>
          <div className="title">Set Up Your dashboard Password To Continue ...</div>
          <br></br>
          <br></br>
          <Input
            type="text"
            classes="icon-username"
            label="Username"
            name="username"
            placeholder="Username/Email"/>
          <Input
            type="password"
            classes=" icon-password"
            label="Username"
            name="username"
            placeholder="Password"/>
          <div className="col-sm-12 login-link">
            <span className="forgot-password">
              <a href="/forgot-password">Forgot Your Password?</a>
            </span>

            <Button type="submit" animate={true} className="login-btn" text="LOG IN" name="" id=""/>
          </div>
          <hr className="col-sm-12  dashboard-or"></hr>
          <button className="col-sm-12 signup-btn">SIGN UP</button>
          </div>
        </form>

      </div>
      </div>
    );
  }
}

export default SignUpConfirm;