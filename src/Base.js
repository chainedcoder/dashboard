import React, {Component} from 'react';
// import logo from './logo.svg';
import './App.css';
import './Grid.css'
// import LogIn from './components/LogIn';
import Grid from '@material-ui/core/Grid';

import RightSideLanding from './components/RightSideLanding';

import {withStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
const styles = theme => ({
  sectionDesktop: {
    display: 'none',
    [
      theme
        .breakpoints
        .up('sm')
    ]: {
      display: 'flex',
      width: '100%'
    }
  },
  sectionMobile: {
    display: 'flex',
    [
      theme
        .breakpoints
        .up('sm')
    ]: {
      display: 'none',
      width: '100%'
    }
  }
})

class Base extends Component {
  render() {
    const {classes} = this.props;
    return (
      <Grid container spacing={0}>

        <div className={classes.sectionDesktop}>
          <Grid className="" item sm={5} lg={4} md={4}>
            <div className="left-landing ">
              {this.props.children}
              <Grid container spacing={0}>
                <Grid className="" item xs={1} sm={2} lg={2} md={2}></Grid>

                <Grid className="" xs={11} sm={10} lg={10} md={10}>
                  <div className="dashboard-footer">Copyright &copy; 2018 dashboard Inc.</div>
                </Grid>
              </Grid>

            </div>
          </Grid>
          <Grid item sm={7} lg={8} md={8}>
            <RightSideLanding classes=""></RightSideLanding>
          </Grid>
        </div>
        <div className={classes.sectionMobile}>
          <Grid className="" item sm={4} lg={4} md={4}>
            <div className="left-landing ">
              {this.props.children}
              <Grid container spacing={0}>
                <Grid className="" item xs={1} sm={2} lg={2} md={2}></Grid>

                <Grid className="" xs={11} sm={10} lg={10} md={10}>
                  <div className="dashboard-footer">Copyright &copy; 2018 dashboard Inc.</div>
                </Grid>
              </Grid>

            </div>
          </Grid>
        </div>

      </Grid>

    );
  }
}
Base.propTypes = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(Base);
