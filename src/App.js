import React, {Component} from 'react';
// import logo from './logo.svg';
import './App.css';
import PrivateRoute from './components/PrivateRoute';
import LogInPage from './pages/LogInPage';
import SignUpPage from './pages/SignUpForm';
import SetPasswordPage from './pages/SetPasswordPage';
import MetaTags from 'react-meta-tags';
import ForgotPasswordPage from './pages/ForgotPasswordPage';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import TransactionsDetail from './pages/TransactionDetail';
import BaseDashboard from './components/BaseDashboard';
import Home from './pages/Home';
import Transactions from './pages/Transactions';
import Customers from './pages/Customers';
import Customer from './pages/Customer';
import CashOut from './pages/CashOut';
import CashoutDetail from './pages/CashoutDetails'
import ContactUs from './pages/ContactUs';
import Help from './pages/Help';
import Settings from './pages/Settings';
import Reports from './pages/Reports';
import Writers from './pages/Writers';
import Chat from './pages/Chat';
import Developers from './pages/Developers'
import {connect} from 'react-redux';
import LogOut from './components/LogOut'
import store from './redux/index.js'

class App extends Component {
  componentDidMount = () => {
  }
  

  render() {    
    
    store.subscribe(() => console.log('Some action took place!! ', store.getState()))
    

    return (
      <Router>
    {<div>
      <MetaTags>
        <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
      </MetaTags>

      <PrivateRoute exact path='/' component={Home} />
      <Route path='/log-in' component={LogInPage}/>
      <Route path='/logout' component={LogOut}/>
      <Route exact path='/sign-up' component={SignUpPage}/>
      <Route exact path='/password-set' component={SetPasswordPage}/>
      <Route exact path='/forgot-password' component={ForgotPasswordPage}/>
      <PrivateRoute
        exact
        path='/base'
        component={BaseDashboard}
        />
      <PrivateRoute
        exact
        path='/home'
        component={Home}
        />
      <PrivateRoute
        exact
        path='/orders'
        component={Transactions}
        />
      <PrivateRoute
        exact
        path='/orders/:id'
        component={TransactionsDetail}
        />
      <PrivateRoute
        exact
        path='/cashout/details/:id'
        component={CashoutDetail}
        />
      <PrivateRoute
        exact
        path='/customers'
        component={Customers}
        />
      <PrivateRoute
        exact
        path='/customers/customer/:id'
        component={Customer}
        />
      <PrivateRoute
        exact
        path='/cashout'
        component={CashOut}
        />
      <PrivateRoute
        exact
        path='/contact-us'
        component={ContactUs}
        />
      <Route exact path='/help' component={Help} />
      <PrivateRoute
        path='/Settings/:tab?'
        component={Settings}
        />
      <PrivateRoute
        exact
        path='/Reports'
        component={Reports}
        />
      <PrivateRoute
        exact
        path='/developers'
        component={Developers}
        />
      <PrivateRoute
        exact
        path='/writers'
        component={Writers}
        />
      <PrivateRoute
        exact
        path='/Chat'
        component={Chat}
      />

    </div>}
  </Router>  
    );
  }
}



const auth = localStorage.getItem('token') ? true : '';
let token = 'ghviuskadfsd'
let user = '{"user":{"user":{"0":{"access_key":"sdddddfsd"}},"first_name":"Test","last_name":"User"}, "token":"dgsgsdgsd"}'
localStorage.setItem('user', user)
localStorage.setItem('token', token)
localStorage.setItem('activeuser', '{"user":{"name":"user"}}')
const mapState = () => ({ authenticated: auth});
export default connect(mapState)(App);
