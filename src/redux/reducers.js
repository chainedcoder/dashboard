import {combineReducers} from 'redux';
import {sessionReducer} from 'redux-react-session';
import {NOTIFICATION_BAR, REMOVE_NOTIFICATION_BAR} from '../constants/Constants.js'

const initialState = {
  logs: [],
  notificationBar: []
};
const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_LOG':
      return {...state, logs: [...state.logs, action.payload]}
    case NOTIFICATION_BAR:
      return {...state, notificationBar: [...state.notificationBar, action.payload]}
    case REMOVE_NOTIFICATION_BAR:
      let {notificationBar, ...tempState} = {...state}
      return {...tempState, notificationBar:[]}

      default:
      return state;
  }
};
// export default rootReducer;


const reducer = combineReducers({
  session: sessionReducer,
  rootReducer: rootReducer
});

export default reducer
