import reducer from "./reducers.js";
import thunkMiddleware from 'redux-thunk';
import tokenMiddleWare from '../Api/tokenMiddleWare'
import { createStore, compose, applyMiddleware} from 'redux';

// const store = createStore(rootReducer);

const store = createStore(reducer, undefined, compose(applyMiddleware(thunkMiddleware, tokenMiddleWare)));

export default store;

