import {NOTIFICATION_BAR, REMOVE_NOTIFICATION_BAR} from '../constants/Constants.js'

export const debugLog = log => ({type: "ADD_LOG", payload: log});

export const showNotificationBar = notificationBar => ({type: NOTIFICATION_BAR, payload: notificationBar});
export const removeNotificationBar = notificationBar => ({type: REMOVE_NOTIFICATION_BAR, payload: {notificationBar}});
