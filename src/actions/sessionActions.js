import {sessionService} from 'redux-react-session';
import API, {ENDPOINTS} from '../Api/Constants'

export const login = (user, history, errState) => {
  return () => {
    API
      .post(ENDPOINTS.login, JSON.stringify(user))
      .then(res => {
        console.log(res);

        if (res.status === 200) {
          // console.log("Success!")
          const {token} = res.data;
          console.log(token)

          sessionService
            .saveSession({token})
            .then(() => {
              sessionService
                .saveUser(res.data)
                .then(() => {
                  history.push('/');
                })
            })
        } else if (res.status === 404) {
          errState = {
            animate: false,
            active: true,
            waitForResponse: false,
            errors: "Invalid credentials"
          }
        } else {
          errState = {
            animate: false,
            active: true,
            waitForResponse: false,
            errors: "Problem logging in. Please try again"
          }
        }
        console.log(res.data);

      })

  };
};

export const logout = (history) => {
  return () => {
    //hit logout api then
    console.log("logout")
    sessionService.deleteSession();
    sessionService.deleteUser();
    history.push('/log-in');
  };
};


export function timeout () {
  return { type: 'SESSION_TIMEOUT' }
}

export const logOut = () => {
  return { type: 'LOGOUT' }
}