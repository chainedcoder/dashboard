import {withRouter} from 'react-router'

export const queryParams = (props) => {
    const paramString = props.location.search
    const params = paramString.split('&')
    let paramObject = {}
    params.forEach(function(param){
      const k = param.slice(0, param.indexOf('='))
      const v = param.slice(param.indexOf('='), param.length)
      paramObject[k] = v
    })
    return withRouter(paramObject)
  }

  export const queryParamsFromLink = (url) => {
    if (!url){
      return {}
    }
    const paramString = url.split("?")[1]
    const params = paramString.split('&')
    let paramObject = {}
    params.forEach(function(param){
      const k = param.slice(0, param.indexOf('='))
      const v = param.slice(param.indexOf('=')+1, param.length)
      paramObject[k] = v
    })
    return withRouter(paramObject)
  }