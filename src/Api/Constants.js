import axios from 'axios';

let token = null
let xsmak = null
try{
  const user = JSON.parse(localStorage.getItem('user'))
  token = ""
} catch (e){

}
let httpReq = axios.create({baseURL: '127.0.0.1:8000/api/v1/', headers: {
  post: {        // can be common or any other method
    'Content-Type': 'application/json',
    'Authorization': token,
  },
  get: {
    'Content-Type': 'application/json',
    'Authorization': token,
  }
}})
// httpReq.defaults.headers.common.Authorization = token
httpReq.defaults.timeout = 10000;
// httpReq.defaults.headers.common.XSMAK = xsmak
export default httpReq;

export const pureAxios = axios.create({baseURL:'', post:{}, get:{}});

export const ENDPOINTS = {
  login: "users/auth/login/",
  signup: "users/auth/user-signup/",
  setPassword: "users/user/set-password/",
  orders: 'orders/all/'
}
