import moment from 'moment'
function createAxiosAuthMiddleware() {
  return () => next => (action) => {
    const time = moment(window.localStorage.getItem('time'))
    const user = JSON.parse(localStorage.getItem('user'))
    
    if (user && user.token !== null && user.token !== undefined && time !== null) {


        console.log("Login Time: ", time, " Diff: ", moment().diff(time, 'seconds') )
      
    } 
    return next(action);

  };
}

const axiosAuth = createAxiosAuthMiddleware();

export default axiosAuth;
