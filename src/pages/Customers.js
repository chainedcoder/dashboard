import React from 'react';
import BaseDashboard from '../components/BaseDashboard';
import CustomersTable from '../components/CustomersTable';

class Customers extends React.Component {
  render() {
    return (
      <div>
        <BaseDashboard>
          <div className="tab-menu tab-active">Client</div>
          <CustomersTable ></CustomersTable>
          
        </BaseDashboard>

      </div>
    );
  }

}

export default Customers;
