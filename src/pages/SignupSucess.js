import React, {Component} from 'react';
// import logo from './logo.svg';
import ForgotPassword from '../components/ForgotPassword';
import Base from '../Base';

class ForgotPasswordPage extends Component {
  render() {
    return (
      <div className="landing">
        <Base>
          <div className="row">
            <div className="col-sm-offset-2">
              <form method="post" className="landing-form col-sm-10 ">
                <div className="row">
                  <div className="spekra-logo"><img
                    src="https://s3.amazonaws.com/dash-user-dashboard/images/white-logo.svg"
                    alt="dashboard logo"/></div>
                  <div className="title-text">You have successfully signed up. We've sent you an confirmation email.</div>
                  <br></br>
                  <br></br>

                  <Button
                    animate={true}
                    className="signup-purple-btn col-sm-12 "
                    text="RESEND EMAIL"
                    name=""
                    id=""/>

                </div>
              </form>

            </div>
          </div>
        </Base>
      </div>
    );
  }
}

export default ForgotPasswordPage;
