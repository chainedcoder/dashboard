import React from 'react';
import BaseDashboard from '../components/BaseDashboard';
import HomeCard from '../components/HomeCard';
import AmountCard from '../components/AmountCard';
import MoneyFlowPill from '../components/MoneyFlowPill';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import DataTable from '../components/DataTable';
import moment from 'moment';
import CashOutModals from '../components/CashOutModals';
// import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as sessionActions from '../actions/sessionActions';
import API, {ENDPOINTS} from '../Api/Constants'
import store from '../redux/index'
import {removeNotificationBar} from '../redux/actions.js'
import Loader from '../components/Loader'

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cashoutOpen: false,
      loading: false,
      moneyFlow: {
        balance: 0,
        this_wk: 0,
        last_wk: 0,
        last_month: 0
      }
    }

    this.handleCashoutOpen = this
      .handleCashoutOpen
      .bind(this);
    this.handleCashoutClose = this
      .handleCashoutClose
      .bind(this);
  }

  handleCashoutOpen = () => {
    this.setState({cashoutOpen: true});
  };

  handleCashoutClose = () => {
    this.setState({cashoutOpen: false});
  };

  handleFetchMoneyFlow = () => {
    this.setState({loading: true})
    API
      .get(ENDPOINTS.moneyFlow, JSON.stringify())
      .then(async(res) => {

        if (res.status === 200) {
          let moneyFlow = res.data.summary
          for (var key in moneyFlow) {
            if (moneyFlow[key]) {
              var tempState = this.state.moneyFlow
              tempState[key] = moneyFlow[key];
              this.setState({moneyFlow: tempState})
            }

          }

          // Object.values(moneyFlow).every(x => (x !== null || x !== ''));

          await window
            .localStorage
            .setItem('currency', res.data.country.currency)

          console.log(res, "MoneyFlow::Home: Recieved moneyFLow")
        }
        if (res.status === 403) {
          this.handleFetchMoneyFlow()
        }
        this.setState({loading: false})

      })
      .catch((error) => {
        this.setState({loading: false})

        if (error.code === 'ECONNABORTED') {
          console.log('Request canceled', error.message);
          console.log('Retrying.....');

          this.handleFetchMoneyFlow()
        } else {
          const res = error.response
          try {
            if (res.status === 403) {
              this
                .props
                .history
                .push('/')
            } else {
              console.log("Hmmm...")
            }

          } catch (e) {
            console.log(e)
          }
        }

      })
  }

  componentDidMount = () => {
    this.handleFetchMoneyFlow()
    store.dispatch(removeNotificationBar({message: 'You have succesfully logged out', timeout: 1}))

    // setInterval(async () => {   const res = fetch('https://www.google.com/', {
    //  mode: 'no-cors'  }).then(     res => {       if(res.status === 200){
    // console.log('Pinged google successfully')       } else {
    // console.log("Connection lost")       }     }   ).catch(err => {
    // console.log(JSON.stringify(err))   }) }, 5000);
  }

  render() {
    // var cashedOutBefore = false
    return (
      <div>
        <BaseDashboard>
          <Loader open={this.state.loading}/>

          <div className="home-date col-sm-12">{moment().format('LLL')}</div>
          <div className="col-sm-12 home-cards">
            <div className='swipe-area'>
              <div className="col-sm-3">
                <Card className={"card "}>
                  <CardContent className={"card-content summary-card bg-blue text white"}>
                    <div className="card-title card-title-blue">My Revenue</div>
                    <div className="card-body text-white">
                      Current balance
                      <div className="card-amount text-white">KES {this
                          .state
                          .moneyFlow
                          .balance
                          .toLocaleString(undefined, {maximumFractionDigits: 2})}</div>
                      {/* <Button onClick={this.handleCashoutOpen} className="bg-white full-width">
                        <span className="button-text">Cash Out</span>
                      </Button> */}
                    </div>

                  </CardContent>
                </Card>
              </div>

              <div className="col-sm-3">
                <HomeCard
                  type="white"
                  title={"This Week"}
                  bgColorClass={""}
                  className="card-options">

                  Total Amount
                  <div className="card-amount ">KES {this
                      .state
                      .moneyFlow
                      .this_wk
                      .toLocaleString(undefined, {maximumFractionDigits: 2})}</div>
                  <MoneyFlowPill
                    gain="KES 67,000"
                    loss="KES 1,400"
                    gainPeriod="Last Week"
                    lossPeriod="Last 3 weeks"/>
                </HomeCard>
              </div>

              <div className="col-sm-3">
                <HomeCard title={"Last Week"} bgColorClass={""} className="card-options">
                  Total Amount
                  <div className="card-amount ">KES {this
                      .state
                      .moneyFlow
                      .last_wk
                      .toLocaleString(undefined, {maximumFractionDigits: 2})}</div>
                  <MoneyFlowPill
                    gain="KES 73,030"
                    loss="KES 16,070"
                    gainPeriod="Last Week"
                    lossPeriod="Last week"/>
                </HomeCard>
              </div>

              <div className="col-sm-3">
                <HomeCard title={"Last Month"} bgColorClass={""} className="card-options">
                  Total Amount
                  <div className="card-amount ">KES {this
                      .state
                      .moneyFlow
                      .last_month
                      .toLocaleString(undefined, {maximumFractionDigits: 2})}</div>
                  <MoneyFlowPill
                    gain="KES 8,100"
                    loss="KES 5,010"
                    gainPeriod="Last 2 Week"
                    lossPeriod="Last week"/>
                </HomeCard>
              </div>
            </div>
          </div>
          <div className="row home-refunds">
            <div className="col-sm-3">
              <HomeCard
                rootClass='refund-section'
                className={"refund-card"}
                title={"Refund Requests"}
                bgColorClass={""}>
                <div className="offset-top-neg-13">
                  <AmountCard
                    amount="78"
                    title="Isaac Kirui"
                    className="refund-close refund-date"
                    date="Transactions"></AmountCard>
                  <AmountCard
                    amount="12"
                    title="Samuel Kamau"
                    className="refund-close refund-date"
                    date="Transactions"></AmountCard>
                  <Card className={"more-card"}>View All Clients</Card>
                </div>

              </HomeCard>
            </div>
            <div className="col-sm-9 table-wrapper">
              <Card className={"recent-trans-card"}>
                <DataTable></DataTable>
              </Card>
            </div>

          </div>
        </BaseDashboard>

        {this.state.cashoutOpen
          ? <CashOutModals
              new={true}
              open={this.state.cashoutOpen}
              onClose={this.handleCashoutClose}/>
          : ""}

      </div>
    )
  }
}

// const { object, bool } = PropTypes;

Home.propTypes = {};

const user = JSON.parse(localStorage.getItem('user'))

const mapState = (state) => ({user: user});

const mapDispatch = (dispatch) => {
  return {
    actions: bindActionCreators(sessionActions, dispatch)
  };
};

export default connect(mapState, mapDispatch)(Home);
