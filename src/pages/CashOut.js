import React from 'react';
import BaseDashboard from '../components/BaseDashboard';
import RecentCashout from '../components/RecentCashOuts';
import Button from '@material-ui/core/Button';
import LoadMore from '../images/icons/LoadMoreIcon.svg';
import CashOutModals from '../components/CashOutModals';
class CashOut extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cashoutOpen: false
    };

    this.handleCashoutOpen = this.handleCashoutOpen.bind(this);
    this.handleCashoutClose = this.handleCashoutClose.bind(this);
  }
  handleCashoutOpen = () => {
    this.setState({cashoutOpen: true});
  };
  handleCashoutClose = () => {
    this.setState({cashoutOpen: false});
  };


  render() {
    

    return (
      <div>
        <BaseDashboard>
          <div className="cashout padded-card  trans-detail">
            <div className="trans-head">
              <div >
                <div>
                  Account Balance
                </div>
                <div>
                  <div className="pull-left">
                    <h1>
                      KSH 213,343.45
                    </h1>
                  </div>
                  <Button
                    className="blue-button cashout-btn" 
                    onClick={this.handleCashoutOpen}
                    >Cash Out</Button>
                </div>
              </div>

            </div>
            <h2>Recent Cash Outs</h2>
            <RecentCashout/>
            <div className="load-more">Load More
              <span className="load-arrow">
                <img className="down-arrow" src={`${LoadMore}`} alt="load more icon"></img>
              </span>
            </div>
          </div>

        </BaseDashboard>

        {this.state.cashoutOpen?<CashOutModals open={this.state.cashoutOpen} onClose={this.handleCashoutClose}/>:""}

      </div>
    )
  }
}

export default CashOut;