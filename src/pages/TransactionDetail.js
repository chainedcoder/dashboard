import React from 'react';
import BaseDashboard from '../components/BaseDashboard';
import Button from '@material-ui/core/Button';
import BackIcon from '../images/icons/Back.svg';
import MpesaIcon from '../images/icons/mpesa_min.png';
import {withRouter} from 'react-router-dom';
import API, {ENDPOINTS} from '../Api/Constants'
import moment from 'moment'
import Loader from '../components/Loader'
import {titleCase} from '../global/functions'
import Refund from '../components/Refund'
import {Upload,Form, message, Icon} from 'antd'

const Dragger = Upload.Dragger

class TransactionDetail extends React.Component {
  constructor() {
    super();
    this.state = {
      amount: '',
      amountEarned: '',
      fee: '',
      date: '',
      status: '',
      id: '',
      names: '',
      phone: '',
      location: '',
      loading: false,
      showRefund: false,
      refundData: {
        amount: '0',
        phone: '25470123456',
        name: 'John Doe'
      },
    };
  }

  formatNumber = (n) => {
    return (parseFloat(n)*100)/100
  }

  getDetails = () => {
    this.setState({loading: true})
    let url = "http://127.0.0.1:8000/orders/" + this.props.match.params.id+'/'

    API
      .get(url)
      .then(res => {
        this.setState({loading: false})
        this.setState(res.data)
        this.setState(res.data)
        console.log(res, "Transaction details")

      })
      .catch((error) => {
        if (error.code === 'ECONNABORTED') {
          console.log('Request canceled', error.message);

          this.getDetails()
        } else {
          this.setState({loading: false})
          console.log("Hmmm...",error)
        }

      })
  }

  handleStatusClicked =(status) => {
    let url = "http://127.0.0.1:8000/orders/" + this.props.match.params.id+"/status/"+status+'/'
    console.log(url)
    this.setState({loading: true})
    API
      .get(url)
      .then(res => {
        this.setState({loading: false})
        console.log(res, "Status changed")
        this.getDetails()

      })
      .catch((error) => {
        if (error.code === 'ECONNABORTED') {
          console.log('Request canceled', error.message);

          this.getDetails()
        } else {
          this.setState({loading: false})
          console.log("Hmmm...",error)
        }

      })
  }

  loadMore = () => {}

  handleCloseRefund = () => {
    this.setState({showRefund: false})
  }

  handleRefundClicked = () => {
    const refundData = {
      phone: this.state.phone,
      name: this.state.names,
      amount: this.state.amountEarned
    }
    this.setState({showRefund: true, refundData: refundData})
  }

  componentDidMount = () => {
    this.getDetails()
  }

  render() {
    let status = 'Pending'
    switch (this.state.status){
      case 1:
        status='Completed';
        break;
      case 0:
          status='Pending';
          break;
      case 2:
          status = 'Delayed';
          break;
      case 3:
        status = 'Cancelled';
        break
      default:
        status = 'Pending';
        break;

    }

    let url = 'http://127.0.0.1:8000/orders/upload/writer/'+ this.props.match.params.id+'/'
    const uploadprops = {
      name: 'file',
      multiple: true,
      action: url,
      method: 'put',
      onChange(info) {
        const status = info.file.status
        if (status !== 'uploading') {
          console.log(info.file, info.fileList)
        }
        if (status === 'done') {
          
          message.success(`${info.file.name} file uploaded successfully.`)
        } else if (status === 'error') {
          message.error(`${info.file.name} file upload failed.`)
        }
      },
    }
    return (
      <BaseDashboard>
        <Refund
          open={this.state.showRefund}
          onClose={this.handleCloseRefund}
          refundData={this.state.refundData}/>

        <Loader open={this.state.loading}/>
        <div >
          <span className="back-button" onClick={this.props.history.goBack}>
            <img
              src={`${BackIcon}`}
              style={{
              'width': 12 + 'px',
              'marginRight': '5px'
            }}
              alt="Back navigation icon"></img>
            Back
          </span>
        </div>
        <div className="padded-card trans-detail">
          <div className="trans-head">
            {/* <div className="col-sm-1">
              <img
                src={`${MpesaIcon}`}
                style={{
                'width': 80 + 'px',
                'marginRight': '4px'
              }}
                alt="Mobile Money provider icon"></img>
            </div> */}
            <div
              className="col-sm-11"
              style={{
              'paddingLeft': '4px'
            }}>
              {/* <div>
                Amount Earned
              </div> */}
              <div>
                <div className="pull-left">
                  <h1>
                    Current Status is: 
                    <span className={status==='Cancelled'?"text-red caps":"text-green caps"}>
                      {status}
                    
                    </span>
                  </h1>
                </div>
                {/* {this.state.status!=='FAILED'?
                <Button className="blue-button" onClick={(amt, phone, name) => {this.handleRefundClicked(amt, phone, name)}}>Refund Payment</Button>
                : ''} */}
                <Button className="small-blue-button" onClick={() => {this.handleStatusClicked('1')}}>Completed</Button>
                <Button className="small-blue-button" onClick={() => {this.handleStatusClicked('0')}}>Pending</Button>
                <Button className="small-blue-button" onClick={() => {this.handleStatusClicked('2')}}>Delayed</Button>
                <Button className="small-blue-button" onClick={() => {this.handleStatusClicked('3')}}>Cancel</Button>

              
                </div>
            </div>
          </div>
          <div >
          <div className="tran-card-title">PAYMENT DETAILS</div>
          <div className="padded-card">
          <Form.Item >
            
                    <Dragger {...uploadprops}>
                      <p className="ant-upload-drag-icon">
                        <Icon type="inbox" />
                      </p>
                      <p className="ant-upload-text">
                        Click or Drag and drop here to upload
                      </p>
                      <p className="ant-upload-hint">
                        Multiple files are supported
                      </p>
                    </Dragger>
                  
                  </Form.Item>
            </div>
            <div className="tran-card-title">PAYMENT DETAILS</div>
            <div className="padded-card">
              <table className="trans-table">
                <tbody>
                  
                  
                  <tr>
                    <td>
                      Deadline:
                    </td>
                    <td>
                      {moment(this.state.deadline).format('lll')}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Order Time:
                    </td>
                    <td>
                      {moment(this.state.created).format('lll')}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Subject:
                    </td>
                    <td>
                      {this.state.subject}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Description:
                    </td>
                    {this.state.description}
                    
                  </tr>
                  <tr>
                    <td>
                      Paper Type:
                    </td>
                    <td className="caps">
                      {this.state.type_of_paper}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Academic Level:
                    </td>
                    <td className="caps">
                      
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Academic Field:
                    </td>
                    <td className="caps">
                      {this.state.academic_field}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Year of Study:
                    </td>
                    <td className="caps">
                    {this.state.year_of_study}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Files: {this.state.files}
                    </td>
                    <td className="caps">
                      
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Top Writer:
                    </td>
                    <td className="caps">
                    {this.state.top_writer? 'Yes' : 'No'}
                       </td>
                  </tr>
                  <tr>
                    <td>
                      citation Style:
                    </td>
                    <td className="caps">
                    {this.state.citation_style}
                      </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div className="customer-detail">
            <div className="tran-card-title ">CUSTOMER DETAILS</div>
            <div className="padded-card">
              <table className="trans-table">
                <tbody>
                  <tr>
                    <td >
                      Name:
                    </td>
                    <td className="text-blue ">
                      {this.state.user}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Email:
                    </td>
                    <td>
                      +{this.state.phone}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Location:
                    </td>
                    <td>
                      {this.state.location}
                    </td>
                  </tr>
                </tbody>
              </table>

            </div>
          </div>
        </div>

      </BaseDashboard>
    );
  }

}

export default withRouter(TransactionDetail);
