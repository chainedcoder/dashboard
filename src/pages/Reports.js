import React from 'react';
import BaseDashboard from '../components/BaseDashboard'

class Reports extends React.Component {
  constructor() {
    super();
    this.state = {
      someKey: 'someValue'
    };
  }

  render() {
    return (
      <div>
        <BaseDashboard></BaseDashboard>

      </div>
    );
  }

  componentDidMount() {
    this.setState({someKey: 'otherValue'});
  }
}

export default Reports;
