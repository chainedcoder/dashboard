import React from 'react';
import BaseDashboard from '../components/BaseDashboard'
// import SmartAvatar from '../components/SmartAvatar';
// import BackIcon from '../images/icons/Back.svg';
import Button from '@material-ui/core/Button';

class Settings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hideSecretKey: true
    }
    this.toggleSecretKey = this
      .toggleSecretKey
      .bind(this)
  }
  toggleSecretKey() {
    this.setState({
      hideSecretKey: !this.state.hideSecretKey
    })

    setTimeout(() => {
      this.setState({hideSecretKey: true});
    }, 10000);
  }
  render() {
    return (
      <div>
        <BaseDashboard>
          {/* <div >
            <span className="back-button" onClick={this.props.history.goBack}>
              <img
                src={`${BackIcon}`}
                style={{
                'width': 12 + 'px',
                'marginRight': '5px'
              }}
                alt="Back navigation icon"></img>
              Back
            </span>
          </div> */}
          <div className="padded-card trans-detail">
            <div className="trans-head">
              <div className="col-sm-1">
                {/* <SmartAvatar names="Moringa School" className="avatar dialog-avatar"></SmartAvatar> */}
              </div>
              <div
                className="col-sm-11"
                style={{
                'paddingLeft': '32px'
              }}>

                <div className="pull-left col-sm-4 api-keys-user-name">
                  <div >
                    <h1>
                      Developer
                    </h1>
                  </div>
                  <div className="cashout-number">
                    Ask us for help
                  </div>
                </div>
                <div
                  className="pull-right col-sm-8"
                  style={{
                  marginTop: 14
                }}>
                  <Button className="blue-button-api">Send</Button>
                </div>
              </div>
            </div>

            {/* <div className="customer-detail">
              <div className="tran-card-title">Test Keys</div>
              <div className="padded-card">
                <table className="trans-table">
                  <tbody>
                    <tr>
                      <td >
                        Secret Key:
                      </td>

                      <td >
                        <div className="tooltip">
                          <span className="tooltiptext">Click to show</span>
                          <span
                            className={this.state.hideSecretKey
                            ? "blur"
                            : ""}
                            onClick={this.toggleSecretKey}>hgMNxYLj5uFNMJgKAmdAYvq0nm67duPx</span>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        Public Key:
                      </td>
                      <td>
                        95Jwul50QsBN8s1rjgyGlk1ACZZgWl8U
                      </td>
                    </tr>
                  </tbody>
                </table>

              </div>
            </div> */}
            {/* <div className="customer-detail">
              <div className="tran-card-title text-blue-api">Live Keys</div>
              <div className="padded-card">
                <table className="trans-table">
                  <tbody>
                    <tr>
                      <td >
                        Secret Key:
                      </td>

                      <td >
                        <div className="tooltip">
                          <span className="tooltiptext">Click to show</span>
                          <span
                            className={this.state.hideSecretKey
                            ? "blur"
                            : ""}
                            onClick={this.toggleSecretKey}>hgMNxYLj5uFNMJgKAmdAYvq0nm67duPx</span>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        Public Key:
                      </td>
                      <td>
                        95Jwul50QsBN8s1rjgyGlk1ACZZgWl8U
                      </td>
                    </tr>
                  </tbody>
                </table>

              </div>
            </div> */}
          </div>
        </BaseDashboard>

      </div>
    )
  }
}

export default Settings;