import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import SimpleInput from '../components/SimpleInput';
import ContactSuccessIcon from '../images/icons/ContactSuccessIcon.svg'
class ContactUs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      formOpen: false,
      success: false,
      mainOpen: true,
      highlite: '',
      successMsg: ''
    }
  }

  handleClose = () => {
    this.setState({success:false, mainOpen:true, formOpen:false, highlite:'', successMsg: ''})
    this
      .props
      .onclose();
    console.log("Closing contact us")
  }

  contactusForm = () => {
    this.setState({formOpen: true})
  }
  callbackReqSuccess = () => {
    this.setState({formOpen: false, mainOpen: false, success: true, highlite:"Request Sent!", successMsg:"We'll give you a call shortly. Make sure you're close to your phone."})
  }

  sendMsgSuccess = () => {
    this.setState({formOpen: false, mainOpen: false, success: true, highlite:"Message Sent!", successMsg:"Your message was sent successfully. We'll get back to you shortly."})
  }

  render() {
    let success = <div className="dialog-body contact-us">
      <div >
        <img className="r-status-icon" src={`${ContactSuccessIcon}`} alt=""></img>
        <div className='contactus-highlite'>{this.state.highlite}</div>
        <div className='contactus-info-status'>{this.state.successMsg}</div>

      </div>
    </div>

    let contactus = (

      <div className="dialog-body contact-us">

        <div className='contactus-highlite'>Need Help?</div>
        <div className='contactus-info'>Request A Callback Or Send Us A Message Using The Buttons Below.</div>

        <div className="contactus-actions row">
          <Button
            onClick={this.callbackReqSuccess}
            className="col-sm-12 contactus-btn contactus-btn-msg">Request A Callback</Button>
          <Button
            onClick={this.contactusForm}
            className="col-sm-12 contactus-btn contactus-btn-callback">Message Us</Button>

        </div>
      </div>
    )

    let contactusForm = (
      <div className="dialog-body contact-us">
        {/* <SimpleInput
                  className="dialog-enter-details"
                  id="subject"
                  label="Subject"
                  type="text"
                  inputClass=""
                  name="subject"
                  placeholder=""/> */}
        <SimpleInput
          className="dialog-enter-details settings-input"
          id="message"
          label="Message"
          type="textarea"
          inputClass="contact-textarea"
          name="message"
          placeholder=""
          maxLength={200}/>
        <Button
          onClick={this.sendMsgSuccess}
          className="col-sm-12 contactus-btn contactus-btn-msg">Send Message</Button>

      </div>

    )

    return (
      <Dialog
        open={this.props.open}
        onClose
        ={this.handleClose}
        aria-labelledby="form-dialog-title"
        className="dialog">
        <div className="contact-us-content">
          <div className="cashout-dialog-title">
            {this.props.title === "undefined"
              ? 'Contact Us'
              : "Contact Us"}
          </div>

          {this.state.formOpen
            ? contactusForm
            : ''}
          {this.state.mainOpen
            ? contactus
            : ''}
          {this.state.success
            ? success
            : contactus}

        </div>

      </Dialog>

    );
  }

}

export default ContactUs;
