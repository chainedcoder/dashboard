import React from 'react';
import BaseDashboard from '../components/BaseDashboard'
import TransactionsTable from '../components/TransactionsTable.js'

class Transactions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: "transactions"
    }

    this.handleTabSwitch = this
      .handleTabSwitch
      .bind(this)
  }

  handleTabSwitch(id, e) {
    const targetName = e.target.id;
    this.setState({activeTab: targetName})
    console.log(id, this.state)

  }

  render() {
    // var tabMenu = <div className={"tab-menu" +
    // this.state.activeTab==="transaction"?"tab-active":""}>Transactions</div>
    return (
      <div>
        <BaseDashboard>
          <div className='tabs'>
            <div
              id="pending"
              className={this.state.activeTab === "pending"
              ? "tab-menu tab-active"
              : "tab-menu "}
              onClick={(e) => this.handleTabSwitch("pending", e)}>Pending</div>
            <div
              id="complete"
              className={this.state.activeTab === "complete"
              ? "tab-menu tab-active"
              : "tab-menu "}
              onClick={(e) => this.handleTabSwitch("complete", e)}>Completed</div>
            <div
              id="cancelled"
              className={this.state.activeTab === "cancelled"
              ? "tab-menu tab-active"
              : "tab-menu "}
              onClick={(e) => this.handleTabSwitch("cancelled", e)}>Canceled</div>
            {/* <div
            id="payroll"
            className={this.state.activeTab === "payroll"
            ? "tab-menu tab-active"
            : "tab-menu "}
            onClick={(e) => this.handleTabSwitch("payroll", e)}>payroll</div> */}
          </div>
          <div class='table-container'>
          <TransactionsTable ></TransactionsTable>
          </div>

        </BaseDashboard>

      </div>
    )
  }
}

export default Transactions;