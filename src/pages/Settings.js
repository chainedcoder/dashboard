import React from 'react';
import BaseDashboard from '../components/BaseDashboard'
import Button from '@material-ui/core/Button';
import SmartAvatar from '../components/SmartAvatar';
import SimpleInput from '../components/SimpleInput';
import CheckoutDialog from '../components/CheckoutDialog';
import {withRouter} from 'react-router-dom';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Input from '@material-ui/core/Input'
import FileUpload from '../components/FileUpload'

class Settings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: typeof this.props.match.params.tab === "undefined"
        ? "settings"
        : this.props.match.params.tab,
      changeSettingOpen: false,
      userType: '',
      uploadLogoOpen: false
    }

    this.handleTabSwitch = this
      .handleTabSwitch
      .bind(this)
    this.handleChangeSetting = this
      .handleChangeSetting
      .bind(this)
    this.handleChangeSettingClose = this
      .handleChangeSettingClose
      .bind(this)
  }

  handleTabSwitch(id, e) {
    const targetName = e.target.id;
    this.setState({activeTab: targetName})
    console.log(id, this.state)

  }
  handleChangeSetting = () => {
    this.setState({changeSettingOpen: true});
  }
  handleChangeSettingClose = () => {
    this.setState({changeSettingOpen: false});
  }

  selectType = (e) => {
    this.setState({userType:e.target.value})

  }

  uploadLogo = () => {
    this.setState({uploadLogoOpen: true})
  }

  closeUploadLogo = () => {
    this.setState({uploadLogoOpen: false})
  }

  render() {
    const profile = <div className="card">
      <div className="settings-title row">
        <div className="tab-menu-settings tab-active">My Profile</div>
        {/* <a href='/logout'><Button className="settings-btn bg-blue text-white pull-right ">Add New User</Button></a> */}
      </div>
      <div className="settings-pic-pass row">
        <SmartAvatar
          names="Brenda Wambui"
          className="settings-prof-pic pull-left col-sm-2"></SmartAvatar>
        <div className="col-sm-10" style={{
          padding: 10
        }}>
          <Button className="settings-btn upload-btn pull-left ">Upload new picture</Button>
          <Button className="settings-btn bg-white text-paleblue pull-left ">Delete</Button>
          <div className="chang-pass pull-right">Password:
            <Button
              className="changepwd-btn bg-white text-blue float-none"
              onClick={this.handleChangeSetting}>Change Password</Button>
          </div>
        </div>
      </div>
      <div
        className="padded-card settings-form row"
        style={{
        margin: 0,
        padding: "24px 35"
      }}>
        <div className="row">
          <div className="col-sm-6"><SimpleInput
            className="dialog-enter-details settings-input"
            id="firstname"
            label="First Name"
            type="text"
            InputClass=""
            name="firstname"
            placeholder="Enter Your First Name"/></div>
          <div className="col-sm-6"><SimpleInput
            className="dialog-enter-details settings-input"
            id="lastname"
            label="Last Name"
            type="text"
            InputClass=""
            name="lastname"
            placeholder="Enter Your Last Name"/></div>
        </div>

        <div className="row">
          <div className="col-sm-6">
            <SimpleInput
              className="dialog-enter-details settings-input"
              id="phonenumber"
              label="Phone Number"
              type="text"
              InputClass=""
              name="phonenumber"
              placeholder="Enter Your Phone Number"/>
          </div>
          <div className="col-sm-6">
            <SimpleInput
              className="dialog-enter-details settings-input"
              id="emailaddr"
              label="Email Address"
              type="text"
              InputClass=""
              name="emailaddr"
              placeholder="Enter Your Email Address"/>
          </div>
        </div>
        <div className="row" style={{
          marginTop: 20
        }}>
          <div className="col-sm-12">
            <Button className="settings-btn bg-white pull-left text-paleblue">Cancel</Button>
            <Button className="settings-btn bg-blue text-white  pull-left">Save</Button>
          </div>
        </div>

        <CheckoutDialog title="Change Password" open={this.state.changeSettingOpen}>
          <div className="col-sm-12">
            <SimpleInput
              className="dialog-enter-details"
              id="oldpass"
              label="Enter Old Password"
              type="password"
              iconClass="dark-pass-vision"
              name="amount"
              placeholder=""
              style={{
              marginTop: '-30px'
            }}/>
            <SimpleInput
              className="dialog-enter-details"
              id="newpass"
              label="Enter New Password"
              type="password"
              iconClass="dark-pass-vision"
              name="newpass"
              placeholder=""
              style={{
              marginTop: '0px'
            }}/>
            <SimpleInput
              className="dialog-enter-details"
              id="accnewpass"
              label="Confirm New Password"
              type="password"
              iconClass="dark-pass-vision"
              name="cnewpass"
              placeholder=""
              style={{
              marginTop: '0px'
            }}/>
          </div>
          <div className="dialog-btn-sec col-sm-12">
            <Button
              className="blue-button dialog-btn bg-white text-paleblue pull-left margin-zero"
              onClick={this.handleChangeSettingClose}>Cancel</Button>
            <Button className="blue-button dialog-btn pull-right text-white-button">Save</Button>

          </div>
        </CheckoutDialog>
      </div>
    </div>

    const user = <div className="card">
      <div className="settings-title row">
        <div className="tab-menu-settings tab-active">Company Settings</div>
        {/* <a href='/logout'><Button className="settings-btn bg-blue text-white pull-right ">Add Company</Button></a> */}
      </div>
      <div className="settings-pic-pass row">
        <SmartAvatar  names="" className="settings-prof-pic pull-left col-sm-2"></SmartAvatar>
        <div className="col-sm-10" style={{
          padding: 10
        }}>
          <Button onClick={this.uploadLogo} className="settings-btn upload-btn pull-left ">Upload Logo</Button>

          {/* <Button className="settings-btn bg-white text-paleblue pull-left ">Delete</Button> */}
          {/* <div className="chang-pass pull-right">Password:
            <Button
              className="changepwd-btn bg-white text-blue float-none"
              onClick={this.handleChangeSetting}>Change Password</Button>
          </div> */}
        </div>
      </div>
      <div
        className="padded-card settings-form row"
        style={{
        margin: 0,
        padding: "24px 35"
      }}>
        {/* <div className="row">
          <div className="col-sm-6"><SimpleInput
            className="dialog-enter-details settings-input"
            id="username"
            label="User Name"
            type="text"
            InputClass=""
            name="username"
            placeholder="Enter Your Legal User Name"/></div>
          <div className="col-sm-6"><SimpleInput
            className="dialog-enter-details settings-input"
            id="city"
            label="User Type"
            type="text"
            InputClass=""
            name="usertype"
            placeholder="Enter Your User Type"/></div>
        </div> */}

        <div className="row">

          <div className='col-sm-6 settings-select'>
            <label htmlFor={this.props.id} className="cashout-label">Country</label>
            <Select
              value={this.state.userType}
              onChange={this.selectType}
              displayEmpty
              name="county"
              className="simple-select  "
              input={< Input name = "name" id = "name-helper" />}>

              <MenuItem value="kenya">China</MenuItem>
              <MenuItem value="ghana">USA</MenuItem>
            </Select>
          </div>

          <div className="col-sm-6">
            <SimpleInput
              className="dialog-enter-details settings-input"
              id="userphone"
              label="Phone Number"
              type="numerical"
              InputClass=""
              name="userphone"
              placeholder="Enter Your Phone Number"/>
          </div>
        </div>
        <div className="row" style={{
          marginTop: 20
        }}>
          <div className="col-sm-12">
            <Button className="settings-btn bg-white pull-left text-paleblue">Cancel</Button>
            <Button className="settings-btn bg-blue text-white  pull-left">Save</Button>
          </div>
        </div>

        <CheckoutDialog title="Change Password" open={this.state.changeSettingOpen}>
          <div className="col-sm-12">
            <SimpleInput
              className="dialog-enter-details"
              id="oldpass"
              label="Enter Old Password"
              type="password"
              iconClass="dark-pass-vision"
              name="amount"
              placeholder=""
              style={{
              marginTop: '-30px'
            }}/>
            <SimpleInput
              className="dialog-enter-details"
              id="newpass"
              label="Enter New Password"
              type="password"
              iconClass="dark-pass-vision"
              name="newpass"
              placeholder=""
              style={{
              marginTop: '0px'
            }}/>
            <SimpleInput
              className="dialog-enter-details"
              id="ccnewpass"
              label="Confirm New Password"
              type="password"
              iconClass="dark-pass-vision"
              name="cnewpass"
              placeholder=""
              style={{
              marginTop: '0px'
            }}/>
          </div>
          <div className="dialog-btn-sec col-sm-12">
            <Button
              className="blue-button dialog-btn bg-white text-paleblue pull-left margin-zero"
              onClick={this.handleChangeSettingClose}>Cancel</Button>
            <Button className="blue-button dialog-btn pull-right text-white-button">Cash Out</Button>

          </div>
        </CheckoutDialog>
      </div>
    </div>

    return (
      <div>
        <BaseDashboard>
        <FileUpload open={this.state.uploadLogoOpen} onClose={this.closeUploadLogo} />
          <div
            id="settings"
            className={this.state.activeTab === "settings"
            ? "tab-menu tab-active"
            : "tab-menu "}
            onClick={(e) => this.handleTabSwitch("settings", e)}>User Settings</div>
          <div
            id="profile"
            className={this.state.activeTab === "profile"
            ? "tab-menu tab-active"
            : "tab-menu "}
            onClick={(e) => this.handleTabSwitch("profile", e)}>Profile Settings</div>
          <div
            id="notification"
            className={this.state.activeTab === "notification"
            ? "tab-menu tab-active"
            : "tab-menu "}
            onClick={(e) => this.handleTabSwitch("notification", e)}>Notification Settings</div>

          {this.state.activeTab === "settings"
            ? user
            : ''}
          {this.state.activeTab === "profile"
            ? profile
            : ''}

        </BaseDashboard>

      </div>
    )
  }
}

export default withRouter(Settings);