import React, { Component } from 'react';
// import logo from './logo.svg';
import SignUp from '../components/SignUp';
import Base from '../Base';


class SignUpPage extends Component {
  render() {
    return (
      <div className="landing">
        <Base><SignUp></SignUp></Base>
      </div>
    );
  }
}

export default SignUpPage;
