import React, { Component } from 'react';
// import logo from './logo.svg';
import ForgotPassword from '../components/ForgotPassword';
import Base from '../Base';


class ForgotPasswordPage extends Component {
  render() {
    return (
      <div className="landing">
        <Base><ForgotPassword></ForgotPassword></Base>
      </div>
    );
  }
}

export default ForgotPasswordPage;
