import React from 'react';
import BaseDashboard from '../components/BaseDashboard';
import Button from '@material-ui/core/Button';
import BackIcon from '../images/icons/Back.svg';
import {withRouter} from 'react-router-dom';
// import API, {ENDPOINTS} from '../Api/Constants'
// import moment from 'moment'

class CashoutDetails extends React.Component {
  constructor() {
    super();
    this.state = { someKey: 'someValue' };
  }

  render() {
    return (
        <BaseDashboard>
        <div  >
          <span className="back-button" onClick={this.props.history.goBack}><img
            src={`${BackIcon}`}
            style={{
            'width': 12 + 'px',
            'marginRight': '5px'
          }}
            alt="Back navigation icon"></img>
          Back
          </span>
        </div>
        <div className="padded-card trans-detail">
          <div className="trans-head">
            <div className="col-sm-1">
              
            </div>
            <div className="col-sm-11" style={{'paddingLeft': '4px'}}>
              <div>
                Amount Widthrawn
              </div>
              <div>
                <div className="pull-left">
                  <h1>
                    KES 13,343.45
                  </h1>
                </div>
                <Button className="blue-button-cashout">Re-Do This Cash Out</Button>
              </div>
            </div> 
          </div>
          <div >
            <div className="tran-card-title">CASHOUT DETAILS</div>
            <div className="padded-card">
              <table className="trans-table">
                <tbody>
                  <tr>
                    <td>
                      Amount:
                    </td>
                    <td className="caps">
                      ksh 13,343.45
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Transaction Fee:
                    </td>
                    <td className="caps">
                      ksh 0.00
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Date/Time:
                    </td>
                    <td>
                      2018/08/21 18:12:42
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Status:
                    </td>
                    <td className="text-green caps">
                      Completed
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Transaction ID:
                    </td>
                    <td className="caps">
                      UT-R2B-OLASD11
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div className="customer-detail">
            <div className="tran-card-title ">CASHOUT METHOD</div>
            <div className="padded-card">
              <table className="trans-table">
                <tbody>
                  <tr>
                    <td >
                      Bank Name:
                    </td>
                    <td className="text-blue ">
                      Standard Chartered Bank
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Bank Account Number:
                    </td>
                    <td>
                      0123456789123
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Bank Branch:
                    </td>
                    <td>
                      Yaya Branch
                    </td>
                  </tr>
                </tbody>
              </table>

            </div>
          </div>
        </div>

      </BaseDashboard>
    );
  }

}

export default withRouter(CashoutDetails);
