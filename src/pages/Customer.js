import React from 'react';
import BaseDashboard from '../components/BaseDashboard';
import RecentCashout from '../components/RecentCashOuts';
import SmartAvatar from '../components/SmartAvatar';
import {withRouter} from 'react-router-dom';
import BackIcon from '../images/icons/Back.svg';
import API, {ENDPOINTS} from '../Api/Constants'
import moment from 'moment'
import Loader from '../components/Loader'
import {titleCase} from '../global/functions'

class Customer extends React.Component {
  constructor() {
    super();
    this.state = {
      amount: '',
      amountPaid: '',
      fee: '',
      date: '',
      status: '',
      id: '',
      names: '',
      phone: '',
      location: '',
      loading: false
    };
  }

  getDetails = () => {
    console.log("Getting details")
    this.setState({loading: true})
    API
      .get(ENDPOINTS.customers + this.props.match.params.id + '/')
      .then(res => {
        console.log(res, "Transaction details")
        this.setState({loading: false})
        if (res.status === 200) {
          const data = res.data;
          const date = moment(data.created).format('LLL');
          const trans_fee = Math.floor((data.m_request_amount - data.m_amount)*100)/100;
          const amount = Math.floor(data
            .m_request_amount*100)/100;
          let fullName = titleCase(data
            .person_name
            .toLowerCase());
          this.setState({
            fee: trans_fee,
            status: data.status,
            id: data.tid,
            date: date,
            phone: data.phone,
            names: fullName,
            amount: amount
          })

        }

      })
      .catch((error) => {
        if (error.code === 'ECONNABORTED') {
          console.log('Request canceled', error.message);
          this.getDetails()
        } else {
          this.setState({loading:false})
          console.log(JSON.stringify(error))
          
        }

      })

  }

  componentDidMount = () => {
    this.getDetails()
  }

  render() {
    return (
      <BaseDashboard>
      <Loader open={this.state.loading}/>
        <div >
          <span className="back-button" onClick={this.props.history.goBack}>
            <img
              src={`${BackIcon}`}
              style={{
              'width': 12 + 'px',
              'marginRight': '5px'
            }}
              alt="Back navigation icon"></img>
            Back
          </span>
        </div>
        <div className="padded-card">
          <div className="trans-head">
            <div className="col-sm-1">
              <SmartAvatar names="Isaac Shidi" className="avatar dialog-avatar"></SmartAvatar>
            </div>
            <div
              className="col-sm-11"
              style={{
              'paddingLeft': '4px'
            }}>

              <div className="pull-left col-sm-4 cashout-name">
                <div >
                  {/* <h1></h1> */}
                </div>
                <div className="cashout-number"></div>
              </div>
              <div
                className="pull-right col-sm-8"
                style={{
                marginTop: 14
              }}></div>
            </div>
          </div>
          <div >
            <div className="tran-card-title-cashout">Payment Details</div>
            <div className="padded-card">
              <table className="trans-table">
                <tbody>
                  <tr>
                    <td>
                      Name:
                    </td>
                    <td className=""></td>
                  </tr>
                  <tr>
                    <td>
                      Customer Since:
                    </td>
                    <td className=""></td>
                  </tr>
                  <tr>
                    <td>
                      Phone Number:
                    </td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>
                      Location:
                    </td>
                    <td className=""></td>
                  </tr>
                  <tr>
                    <td>
                      Transactions:
                    </td>
                    <td className="caps"></td>
                  </tr>

                </tbody>
              </table>
            </div>
          </div>

          <div
            className="tran-card-title-cashout"
            style={{
            padding: "20px 0 0 0"
          }}>Recent Transactions</div>
          <RecentCashout maxRows={3}/>
        </div>
      </BaseDashboard>
    );
  }

}

export default withRouter(Customer);
