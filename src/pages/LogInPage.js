import React, { Component } from 'react';
// import logo from './logo.svg';
import LogIn from '../components/LogIn';
import Base from '../Base';

class LogInPage extends Component {
  render() {
    return (
      <div className="landing">
        <Base><LogIn></LogIn></Base>
      </div>
    );
  }
}

export default LogInPage;
