
export const NOTIFICATION_BAR = 'NOTIFICATION_BAR'
export const REMOVE_NOTIFICATION_BAR = "REMOVE_NOTIFICATION_BAR"

export const ENDPOINTS = {
    login: "users/auth/login/",
    signup: "users/auth/user-signup/",
    setPassword: "users/user/set-password/",
    transactions: "wallets/transaction/",
    recentTrans: "wallets/transaction/?stype=C2M&limit=5&status=SUCCESSFUL",
    users: "users/user/",
    userEdit: "users/user/edit/",
    userEditLogo: "users/user-logo/",
    userEditAvator: "users/edit-avator/",
    moneyFlow: 'users/user/my-user/',
    customers: 'people/person/',
    refreshToken:  'users/user/refresh/'
  }
  
  